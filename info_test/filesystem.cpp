#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QFileInfoList>
#include <QString>

void ProjectManager::emptyFolder(QDir dir)
{
	QFileInfoList list = dir.entryInfoList();
	QFileInfo info;
	QString path, name;
	int i;

	for(i=0; i < list.count(); i++)
	{
		info = list[i];
		name = list[i].fileName();
		if(name == "." || name == "..") continue;

		path = list[i].absoluteFilePath();
		if(info.isDir())
		{
			emptyFolder(QDir(path));
			dir.rmdir(path);
		}
		else dir.remove(path);
	}
}

void ProjectManager::copyFolder(QDir src, QDir dst)
{
	QFileInfoList list = src.entryInfoList();
	QFileInfo info;
	QString path, name;
	QString srcpath = src.absolutePath() + "/";
	QString dstpath = dst.absolutePath() + "/";
	int i;

	for(i=0; i < list.count(); i++)
	{
		info = list[i];
		name = list[i].fileName();
		if(name == "." || name == "..") continue;

		path = list[i].absoluteFilePath();
		if(info.isDir())
		{
			dst.mkdir(name);

			copyFolder(QDir(srcpath + name), QDir(dstpath + name));
		}
		else QFile(srcpath + name).copy(dstpath + name);
	}
}
