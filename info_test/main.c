#include <stdio.h>
#include <libfx.h>
#include <math.h>

struct Point
{
	int x;
	int y;
};

int main(int argc, char **argv)
{
	unsigned i;
	struct Point p[5];

	for(i=0;i<5;i++)
	{
		double angle = 6.28*i/5 + 1.57;
		p[i].x = cos(angle);
		p[i].y = sin(angle);
	}

	fx_clear();
	for(i=0;i<5;i++) fx_line(p[i],p[(i+2)%5]);
	fx_waitkey();

	return 0;
}
