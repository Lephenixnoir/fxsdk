/*
	fxSDK sample program.
	Draws a star on the screen and returns.
*/



/*
	Headers inclusion.
*/

#include <fxlib.h>



/*
	Point structure definition.
*/

struct Point
{
	int x;
	int y;
};



/*
	main()

	Main function. Called by initialization script.

	@return		0 on error, 1 otherwise.
*/

int main(void)
{
	// Using an iterator and a key variable.
	unsigned i, key;
	// Using five points to draw a star.
	struct Point p[5];

	// Computing the coordinates of each point.
	for(i = 0; i < 5; i++)
	{
		// Getting the right angle (with a point at the top).
		double angle = 6.28 * i / 5 - 1.57;
		// The star is centered on the screen, at (64 ; 32).
		// Its radius is 24 pixels.
		p[i].x = 24 * cos(angle) + 64;
		p[i].y = 24 * sin(angle) + 32;
	}

	// Clearing the video ram and drawing the segments.
	Bdisp_AllClr_VRAM();
	for(i = 0; i < 5; i++)
	{
		Bdisp_DrawLineVRAM(p[i].x, p[i].y, p[(i+2)%5].x, p[(i+2)%5].y);
	}

	// Waiting for a keyboard event, implicitly refreshing the screen.
	GetKey(&key);

	// Successfully returning from program.
	return 1;
}
