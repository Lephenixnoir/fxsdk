######################################################################
# Automatically generated by qmake (3.0) ven. mai 22 19:05:41 2015
######################################################################

TEMPLATE = app
TARGET = fxSDK
INCLUDEPATH += . include
QT += widgets
QMAKE_CXXFLAGS += -std=c++11

# Input
HEADERS += include/CodeEdit.hpp \
           include/DockWidget.hpp \
           include/DynamicMenu.hpp \
           include/DynamicMenuBar.hpp \
           include/EditorFont.hpp \
           include/EditorSet.hpp \
           include/EditorText.hpp \
           include/EditorWidget.hpp \
           include/Highlighter.hpp \
           include/Icon.hpp \
           include/MainWindow.hpp \
           include/Project.hpp \
           include/ProjectConfigDialog.hpp \
           include/ProjectManager.hpp \
           include/ProjectView.hpp \
           include/Resource.hpp \
           include/StyleParser.hpp
SOURCES += src/CodeEdit.cpp \
           src/DockWidget.cpp \
           src/DynamicMenu.cpp \
           src/DynamicMenuBar.cpp \
           src/EditorFont.cpp \
           src/EditorSet.cpp \
           src/EditorText.cpp \
           src/EditorWidget.cpp \
           src/Highlighter.cpp \
           src/Icon.cpp \
           src/main.cpp \
           src/MainWindow.cpp \
           src/Project.cpp \
           src/ProjectConfigDialog.cpp \
           src/ProjectManager.cpp \
           src/ProjectView.cpp \
           src/Resource.cpp \
           src/StyleParser.cpp
