#include "EditorWidget.hpp"

EditorWidget::EditorWidget(QWidget *parent, QString resourcePath)
	: QWidget(parent), _changed(false), _resource(resourcePath), _menu(0)
{
}

bool EditorWidget::changed(void)
{
	return _changed;
}

DynamicMenu *EditorWidget::dynamicMenu(void)
{
	return _menu;
}

QString EditorWidget::file(void)
{
	return _resource;
}

void EditorWidget::setChanged(bool hasChanged)
{
	_changed = hasChanged;
}

void EditorWidget::setDynamicMenu(DynamicMenu *new_menu)
{
	_menu = new_menu;
}

void EditorWidget::setResource(QString new_resource)
{
	_resource = new_resource;
}

QString EditorWidget::openFile(bool saveas)
{
	if(!_resource.length() || saveas)
	{
		QFileDialog d;
		d.setFileMode(QFileDialog::AnyFile);
		d.setAcceptMode(QFileDialog::AcceptSave);
		d.setNameFilter("All files (*)");
		d.exec();

		if(!d.result()) return "";
		_resource = d.selectedFiles()[0];
	}

	return _resource;
}
