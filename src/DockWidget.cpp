#include "DockWidget.hpp"

DockWidget::DockWidget(QString title, Qt::Orientation o, QWidget *parent)
	: QDockWidget(title,parent)
{
	setFloating(false);
	setFeatures(QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable);
	setTitleBarWidget(new QWidget(this));

	sub = new SubWidget(title,o,this);
	setWidget(sub);
}

void DockWidget::addWidget(QWidget *w)
{
	sub->addWidget(w);
}

void DockWidget::addSpacing(int spacing)
{
	sub->addSpacing(spacing);
}

void DockWidget::addLayout(QLayout *layout)
{
	sub->addLayout(layout);
}

void DockWidget::setName(QString name)
{
	sub->setName(name);
}

DockWidget::SubWidget::SubWidget(QString name, Qt::Orientation o, DockWidget *parent)
	: QWidget(parent), _dock(parent)
{
	_name = name;
	setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Preferred);

	_main = new QWidget(this);
	_main->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Expanding);
	_main->setGeometry(0,14,width(),height()-14);

	_layout = new QBoxLayout(
		o == Qt::Vertical ? QBoxLayout::TopToBottom : QBoxLayout::LeftToRight,_main);
	_layout->setMargin(4);
}

void DockWidget::SubWidget::addWidget(QWidget *widget)
{
	_layout->addWidget(widget);
}

void DockWidget::SubWidget::addSpacing(int spacing)
{
	_layout->addSpacing(spacing);
}

void DockWidget::SubWidget::addLayout(QLayout *layout)
{
	_layout->addLayout(layout);
}

void DockWidget::SubWidget::setName(QString name)
{
	_name = name;
	update();
}

void DockWidget::SubWidget::mousePressEvent(QMouseEvent *event)
{
	if(QRect(width()-13,5,9,9).contains(event->pos())
		&& event->button() == Qt::LeftButton)
	{
		_dock->close();
	}
}

void DockWidget::SubWidget::resizeEvent(QResizeEvent *)
{
	_main->setGeometry(0,14,width(),height()-14);
}

void DockWidget::SubWidget::paintEvent(QPaintEvent *)
{
	QPainter p(this);

	p.save();
	QFont f = p.font();
	f.setPointSize(7);
	p.setFont(f);
	p.setPen(QColor(0x62,0x67,0x64));
	p.drawText(6,13,_name);
	p.restore();

	p.drawPixmap(width()-14,6,QPixmap("icons/_dock_close.png"));
}
