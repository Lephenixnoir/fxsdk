#include "EditorText.hpp"

EditorText::EditorText(QWidget *parent, QString resource)
	: EditorWidget(parent, resource)
{
	QVBoxLayout *layout = new QVBoxLayout(this);
	edit = new CodeEdit(this);
	setSyntaxHighlighter(resource);

	if(!resource.isEmpty())
	{
		QFile f(resource);
		if(f.open(QFile::ReadOnly|QFile::Text))
		{
			edit->setPlainText(trUtf8(f.readAll()));
			f.close();
			setResource(resource);
		}
	}

	layout->addWidget(edit);
	setFocusProxy(edit);

	edit->setTabStopWidth(QFontMetrics(edit->font()).width('9') * 8);

	DynamicMenu *menu = new DynamicMenu(this);

	QAction *action_save = menu->newAction(Icon::Save,"Save","Ctrl+S",this,
		DynamicCategory::Save);
	action_save->setShortcutContext(Qt::WidgetWithChildrenShortcut);
	connect(action_save,SIGNAL(triggered()),this,SLOT(save()));
	addAction(action_save);

	QAction *action_saveas = menu->newAction(Icon::SaveAs,"Save As...","Shift+Ctrl+S",
		this,DynamicCategory::Save);
	action_saveas->setShortcutContext(Qt::WidgetWithChildrenShortcut);
	connect(action_saveas,SIGNAL(triggered()),this,SLOT(saveAs()));
	addAction(action_saveas);

	QAction *action_find = menu->newAction(Icon::Search,"Find...","Ctrl+F",this,
		DynamicCategory::Edit);
	action_find->setShortcutContext(Qt::WidgetWithChildrenShortcut);
	connect(action_find,SIGNAL(triggered()),this,SLOT(find()));
	addAction(action_find);

	setDynamicMenu(menu);

	connect(edit,SIGNAL(textChanged(void)),this,SLOT(setChanged(void)));
}

void EditorText::setChanged(bool hasChanged)
{
	EditorWidget::setChanged(hasChanged);
}

void EditorText::save(bool saveas)
{
	QString path = EditorWidget::openFile(saveas);
	if(path.isEmpty()) return;

	QFile file(path);
	if(!file.open(QFile::WriteOnly)) return;
	file.write(edit->toPlainText().toUtf8().constData());
	file.close();

	setChanged(false);
	setSyntaxHighlighter(path);
	emit nameChanged(QFileInfo(path).fileName());
}

void EditorText::saveAs(void)
{
	save(true);
}

void EditorText::find(void)
{
	printf("Find !\n");
}

void EditorText::setSyntaxHighlighter(QString resource)
{
	QString ext = QFileInfo(resource).suffix();
	QString language_file = "";

	if(ext == "s" || ext =="S") language_file = "asm.lang";
	else if(ext == "c" || ext == "cpp" || ext == "cxx" || ext == "C"
		|| ext == "h" || ext == "hpp" || ext == "hxx" || ext == "H")
		language_file = "cpp.lang";
	edit->changeSyntaxHighlighter(language_file);
}
