/*
	Headers inclusion.
*/

// This declaration allows to get the icons definition without duplication
// the icon name list (which changes constantly).
#define __ICONSOURCE
// Using the Icon module header.
#include "Icon.hpp"



/*
	Static symbols declarations and static function prototypes.
*/

// Icon left margin : it appeared not to be possible to apply a left margin to
// icons, so the solution was to add space inside them.
static int icon_margin;

// This function extracts an icon from a big icon set bitmap and loads it into
// a QIcon object (adding the margin).
static QIcon loadIcon(QImage i, int x, int y);



/*
	Static functions definitions.
*/

/*
	loadIcon()

	Loads an icon from an image, at the given position (x and y are
	expected to represent the column and row of the icon in an 8*8 grid).

	@param	i	Image to load icon from.
	@param	x	Icon column.
	@param	y	Icon row.

	@return		Icon loaded from pixmap section.
*/

static QIcon loadIcon(QImage i, int x, int y)
{
	// This image will be the final one, padded and at the right size.
	QImage img(8 + icon_margin, 8, QImage::Format_ARGB32);
	// This is a temporary pixmap, loaded from the icon grid.
	QPixmap pixmap = QPixmap::fromImage(i);
	// This painter allows to draw the icon on the final image.
	QPainter painter(&img);

	// Filling by default the final image with transparency.
	img.fill(Qt::transparent);
	// Painting on it an extracted pixmap from the icon grid.
	painter.drawPixmap(icon_margin, 0, pixmap.copy(x << 3, y << 3, 8, 8));
	// Ending the painting operation.
	painter.end();

	// Returning a QIcon loaded from the pixmap.
	return QIcon(QPixmap::fromImage(img));
}



/*
	Icon namespace functions definitions.
*/

/*
	Icon::init()

	Initializes the icon module. Loads the various icons from the given
	icon set file address.

	@param	file	Icon set file address.
	@param	color	Icon color.
	@param	margin	Icon left margin.
*/

void Icon::init(QString file, QColor color, int margin)
{
	// Loading an image from the file.
	QImage i(file);
	// Setting the global icon margin.
	icon_margin = margin;

	// Defining the image colors. This takes advantage of the monochromatic
	// format of the icon set image.
	i.setColor(0, color.rgba());
	i.setColor(1, Qt::transparent);

	/*
		Loading the various icons.
	*/

	// Loading file-related icons.
	NewFile		= loadIcon(i, 0, 0);
	OpenFile	= loadIcon(i, 1, 0);
	Save		= loadIcon(i, 3, 0);
	SaveAs		= loadIcon(i, 4, 0);
	Quit		= loadIcon(i, 6, 0);
	Exit		= loadIcon(i, 7, 0);

	// Loading edition-related icons.
	Search		= loadIcon(i, 0, 1);
	Find		= loadIcon(i, 1, 1);

	// Loading project-related icons.
	NewProject	= loadIcon(i, 0, 2);
	OpenProject	= loadIcon(i, 1, 2);
	Build		= loadIcon(i, 3, 2);
	Settings	= loadIcon(i, 4, 2);

	// Loading view-related icons.
	Menu		= loadIcon(i, 0, 3);
	Fullscreen	= loadIcon(i, 1, 3);

	// Loading resource icons.
	Code		= loadIcon(i, 0, 4);
	Header		= loadIcon(i, 1, 4);
	Library		= loadIcon(i, 2, 4);
	Image		= loadIcon(i, 3, 4);
	Font		= loadIcon(i, 4, 4);
	Other		= loadIcon(i, 5, 4);

	// Loading other icons.
	Documentation	= loadIcon(i, 8, 2);
	Remove          = loadIcon(i, 8, 3);
}
