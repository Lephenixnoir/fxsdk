/*
	Header inclusions.
*/

#include "Resource.hpp"



/*
	Resource class definition.
*/



/*
	Resource::Resource()

	Constructs a Resource object. Automatically detects the resource type
	based on the file extension.

	@param	path	File path.
*/

Resource::Resource(QString path)
{
	// Using a QFileInfo object to retrieve informations.
	QFileInfo info(path);

	// Using a string to store the file extensions.
	QString ext = info.suffix();

	// Setting the resource path.
	_path = info.absoluteFilePath();

	// Determining the resource type using the file extension.
	// Handling source files.
	if(ext == "s" || ext =="S" || ext == "c" || ext == "cpp" ||
		ext == "cxx" || ext == "C") _type = Source;
	// Handling header files.
	else if(ext == "h" || ext == "hpp" || ext == "hxx" || ext == "H")
		_type = Header;
	// Handling libraries.
	else if(ext == "a") _type = Library;
	// Handling images.
	else if(ext == "png" || ext == "jpg" || ext == "gif" || ext == "bmp")
		_type = Image;
	// Handling fonts.
	else if(ext == "fxf") _type = Font;
	// If the extension doesn't match any format, it is interpreted as
	// raw binary data.
	else _type = Other;
}



/*
	Resource::path()

	Returns the absolute path of a resource file.

	@return		File path.
*/

QString Resource::path(void)
{
	// Returning the file path.
	return _path;
}

/*
	Resource::setPath()

	Changes the path of a resource.

	@param	new_path	New file path.
*/

void Resource::setPath(QString new_path)
{
	// Setting the new file path.
	_path = new_path;
}



/*
	Resource::type()

	Returns a resource type.

	@return		Resource type (enum Resource::Type representation).
*/

enum Resource::Type Resource::type(void)
{
	return _type;
}

/*
	Resource::setType()

	Changes a resource's type.

	@param	type	New type (enum Resource::Type representation).
*/

void Resource::setType(enum Type type)
{
	// Setting the new type.
	_type = type;
}
