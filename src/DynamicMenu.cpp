#include "DynamicMenu.hpp"

DynamicMenu::DynamicMenu(QObject *parent)
	: QObject(parent)
{
	elements = QList<struct Element>();
}

void DynamicMenu::addAction(QAction *action, int category)
{
	struct Element el;
	el.type = Action;
	el.action = action;
	el.category = category;

	elements << el;
}

QAction *DynamicMenu::newAction(QString name, QObject *parent, int category)
{
	QAction *action = new QAction(name,parent);
	addAction(action,category);
	return action;
}

QAction *DynamicMenu::newAction(QIcon icon, QString name, QString shortcut,
	QObject *parent, int category)
{
	QAction *action = new QAction(icon,name,parent);
	action->setShortcut(shortcut);
	addAction(action,category);
	return action;
}

void DynamicMenu::addSeparator(int category)
{
	struct Element el;
	el.type = Separator;
	el.pointer = 0;
	el.category = category;

	elements << el;
}

void DynamicMenu::addMenu(QMenu *menu, int category)
{
	struct Element el;
	el.type = Menu;
	el.menu = menu;
	el.category = category;

	elements << el;
}

QMenu *DynamicMenu::newMenu(QWidget *parent, int category)
{
	QMenu *menu = new QMenu(parent);
	addMenu(menu,category);
	return menu;
}

QMenu *DynamicMenu::newMenu(QString name, QWidget *parent, int category)
{
	QMenu *menu = new QMenu(name,parent);
	addMenu(menu,category);
	return menu;
}

void DynamicMenu::addCategory(int categoryId)
{
	if(!categoryId) return;
	int i;
	for(i=0;i<elements.count();i++)
		if(elements[i].type == Category && elements[i].categoryId == categoryId) return;

	struct Element el;
	el.type = Category;
	el.categoryId = categoryId;
	el.category = 0;

	elements << el;
}

QList<struct DynamicMenu::Element> DynamicMenu::_elements(void)
{
	return elements;
}
