#include "ProjectConfigDialog.hpp"

ProjectConfigDialog::ProjectConfigDialog(Project *project, QWidget *parent)
	: QDialog(parent), _project(project)
{
	buttons = new QDialogButtonBox( QDialogButtonBox::Ok
		| QDialogButtonBox::Cancel, Qt::Horizontal, this);
	connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
	connect(buttons, SIGNAL(accepted()), this, SLOT(write()));
	connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));

	setWindowTitle("Project configuration - fxSDK");

	QFormLayout *l = new QFormLayout(this);
	line_name = new QLineEdit(this);
	line_ccopt = new QLineEdit(this);
	line_ldopt = new QLineEdit(this);

	connect(line_name, SIGNAL(textChanged(QString)), this, SLOT(check()));

	if(project)
	{
		line_name->setText(_project->name());
		line_ccopt->setText(_project->flags_cc());
		line_ldopt->setText(_project->flags_ld());
	}

	l->addRow(new QLabel("<b>Project meta data</b>", this));
	l->addRow("Project name", line_name);

	l->addItem(new QSpacerItem(0, 16));
	l->addRow(new QLabel("<b>Build settings</b>", this));
	l->addRow("Compiler options", line_ccopt);
	l->addRow("Linker options", line_ldopt);
	l->addRow(buttons);

	resize(400,sizeHint().height());
}

void ProjectConfigDialog::check(void)
{
	QPushButton *b = buttons->button(QDialogButtonBox::Ok);
	b->setEnabled(false);

	if(line_name->text().isEmpty()) return;

	b->setEnabled(true);
}

void ProjectConfigDialog::write(void)
{
	_project->setName(line_name->text());
	_project->setFlags_cc(line_ccopt->text());
	_project->setFlags_ld(line_ldopt->text());
	_project->save();
}
