#include "StyleParser.hpp"

StyleSheet::StyleSheet(QString r)
	: rules(r)
{
}

void StyleSheet::addTheme(struct Theme t)
{
/*
	printf(
		"addTheme() recap\n"
		"================\n\n"
		"name: %s\n"
		"vars:\n",
		t.name.toUtf8().constData());
	for(int i=0;i<t.vars.count();i++) printf("  %s=%s\n",
		t.vars[i].name.toUtf8().constData(),t.vars[i].value.toUtf8().constData());
	printf("\n\n\n");
*/
	themes << t;
}

void StyleSheet::addRules(QString r)
{
	rules += r + "\n";
}

QString StyleSheet::theme(QString name)
{
	int i,j,n;

	for(i=0;i<themes.count();i++) if(themes[i].name == name)
	{
		QString style = rules;
		QList<struct Variable> vars = themes[i].vars;

		while((n = style.indexOf('@'))+1)
		{
			int k = n+1;
			while(k<style.length() && style[k].isLetter()) k++;
			QString at = style.mid(n,k-n);

			for(j=0;j<vars.count();j++)	if(at == "@"+vars[j].name)
				style = style.left(n) + vars[j].value + style.mid(n+at.length());
		}

		return style;
	}

	return "";
}





StyleSheet StyleParser::parse(QString filepath)
{
	QFile f(filepath);
	StyleSheet stylesheet;
	if(!f.open(QFile::ReadOnly)) return stylesheet;

	QStringList lines = QString(f.readAll()).split('\n');
	f.close();

	struct StyleSheet::Theme theme;
	theme.name = "";
	QString line;
	int i;

	for(i=0;i<lines.count();i++)
	{
		line = lines[i];

		if(theme.name.isEmpty())
		{
			if(line[0] == '<' && line[line.length()-1] == '>')
			{
				theme.name = line.mid(1,line.length()-2).trimmed();
				theme.vars.clear();
			}
			else stylesheet.addRules(line);
		}
		else
		{
			if(line == "</"+theme.name+">") stylesheet.addTheme(theme), theme.name = "";
			else
			{
				struct StyleSheet::Variable var = parseThemeLine(line);
				if(!(var.name.isEmpty() || var.value.isEmpty()))
					theme.vars << var;
			}
		}
	}

	return stylesheet;
}

struct StyleSheet::Variable StyleParser::parseThemeLine(QString line)
{
	struct StyleSheet::Variable var;
	var.name = "";
	var.value = "";

	if(line[0]=='#') return var;
	QStringList elements = line.split('=');
	if(elements.count() != 2) return var;
	var.name = elements[0].trimmed().mid(1,elements[0].length()-1);
	var.value = elements[1].trimmed();

	return var;
}
