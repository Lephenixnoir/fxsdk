/*
	Header inclusions.
*/

#include "ProjectView.hpp"



/*
	ProjectView::ProjectView()

	Constructs a ProjectView widget, inheriting the DockWidget.

	@arg	parent	Parent widget.
*/

ProjectView::ProjectView(QWidget *parent)
	: DockWidget("Project", Qt::Vertical, parent), _project(0)
{
	// Creating the layout.
	_layout = new QVBoxLayout();

	// Adding category widgets to the list.
	ctgys << new CategoryWidget(Icon::Code, "Sources", this);
	ctgys << new CategoryWidget(Icon::Header, "Headers", this);
	ctgys << new CategoryWidget(Icon::Library, "Libraries", this);
	ctgys << new CategoryWidget(Icon::Image, "Images", this);
	ctgys << new CategoryWidget(Icon::Font, "Fonts", this);
	ctgys << new CategoryWidget(Icon::Other, "Others", this);

	// Using a button to trigger configuration dialog.
	QPushButton *settings = new QPushButton("Settings", this);
	// Connecting it to the configure() slot that will call the dialog.
	connect(settings, SIGNAL(clicked()), this, SLOT(configure()));

	// Using a button to save temporary projects, masked if the displayed
	// project is already saved anywhere on disk.
	saveButton = new QPushButton("Save project", this);
	// Connecting it to the corresponding slot.
	connect(saveButton, SIGNAL(clicked()), this, SLOT(saveProject()));

	// Adding spacing between the dock widget title and the list.
	addSpacing(8);
	// Adding the empty layout, that will contain category and resource
	// widgets as soon as the view will be updated.
	addLayout(_layout);
	// Adding the configuration button.
	addWidget(settings);
	// Adding the project save button.
	addWidget(saveButton);
}

/*
	ProjectView::openFile()

	Emits a file opening request on double click on a resource widget.

	@arg	path	File path.
	@arg	type	Resource type (specifies which editor will be used).
*/

void ProjectView::openFile(QString path, enum Resource::Type type)
{
	// Emitting a signal.
	emit fileOpenRequest(path, type);
}



/*
	ProjectView::updateView()

	Destroys and re-generates the content of the dock widget according to
	the given project information.

	@param	project		Project to display. May be NULL.
*/

void ProjectView::updateView(Project *project)
{
	// Using a resource list.
	QList<Resource *> res;
	// Using two iterators.
	int i, j;

	// Setting the current project.
	_project = project;

	// Settings the project name.
	setName(_project->name());

	// Removing all the items in the layout.
	while(_layout->takeAt(0));
	// Deleting all the currently allocated resource widget.
	while(!resw.isEmpty()) delete resw.takeFirst();

	// Testing project pointer validity.
	if(!_project) return;

	// Getting the list of the current project's resources.
	res = _project->resources();

	// Iterating over the categories.
	for(i=0; i<6; i++)
	{
		// Adding a category widget.
		_layout->addWidget(ctgys[i]);

		// Iterating over the resources and matching those with the
		// current type.
		for(j=0; j<res.count(); j++) if(res[j]->type() == i)
		{
			// Creating a new resource widget.
			ResourceWidget *r = new ResourceWidget(res[j], this);
			// Adding it to the widget list.
			resw << r;
			// Adding the current widget to the layout.
			_layout->addWidget(r);

			// Connecting signals.
			connect(r, SIGNAL(resourceRemoveRequest(QString)),
			this, SLOT(removeResource(QString)));
			connect(r, SIGNAL(openFileRequest(QString, enum
			Resource::Type)), this, SLOT(openFile(QString, enum
			Resource::Type)));
		}
	}

	// Adding spacing to prevent resource widgets from spanning all the
	//  available height.
	_layout->addSpacing(1000);
	// Showing or hiding the save button depending on the temporary status
	// of the currently opened project.
	saveButton->setVisible(project->isTemporary());

	// Updating the view.
	update();
}

void ProjectView::configure(void)
{
	ProjectConfigDialog d(_project, this);
	d.exec();

	if(d.result()) updateView(_project);
}

void ProjectView::saveProject(void)
{
	QFileDialog d;
	QString path;

	d.setWindowTitle("Save the current project in...");
	d.setFileMode(QFileDialog::Directory);
	d.setAcceptMode(QFileDialog::AcceptOpen);
	d.exec();
	if(!d.result()) return;

	path = d.selectedFiles()[0];
	_project->setPath(path);
	_project->save();

	updateView(_project);
}

void ProjectView::removeResource(QString path)
{
	if(path.isEmpty() || !_project) return;

	_project->removeResource(path);
	_project->save();

	updateView(_project);
}



ProjectView::CategoryWidget::CategoryWidget(QIcon icon, QString name,
	QWidget *parent) : QWidget(parent)
{
	setFixedHeight(15);
	_icon = icon;
	_name = name;
}

void ProjectView::CategoryWidget::paintEvent(QPaintEvent *)
{
	QPainter p(this);

	_icon.paint(&p, 0, 4, 14, 100, Qt::AlignLeft | Qt::AlignTop);
	p.drawText(18, 0, width(), height(), Qt::AlignVCenter, _name);
}



/*
	ResourceWidget class methods definition.
*/

/*
	ResourceWidget::ResourceWidget()

	Constructs a new resource widget from a resource pointer.
*/

ResourceWidget::ResourceWidget(Resource *resource, QWidget *parent)
	: QLabel(parent)
{
	// Setting the label text.
	setText(QFileInfo(resource->path()).fileName());

	// Setting the resource widget style sheet. The font size property must
	// be explicitly set, otherwise the size hint below computes with the
	// default font size.
	setStyleSheet(
		"QLabel { border-radius: 4px; margin-left: 16px;"
			"padding: 2px 6px; font-size: 9px; }"
		"QLabel:hover { background: #353936; }"
	);

	// Setting its size as fixed to ensure that the light background at
	// hover will have the correct size.
	setFixedSize(minimumSizeHint());

	// Setting the resource pointer.
	_resource = resource;
}

/*
	ResourceWidget::remove()

	Emits a signal to remove file from project.
*/

void ResourceWidget::remove(void)
{
	// Checking attribute validity.
	if(!_resource) return;
	// Emitting a signal in order to open file.
	emit resourceRemoveRequest(_resource->path());
}

/*
	ResourceWidget::mouseDoubleClickEvent()

	Emits a file opening request in answer to a double click event.

	@arg	[anonymous]	Mouse event structure.
*/

void ResourceWidget::mouseDoubleClickEvent(QMouseEvent *)
{
	// Checking attribute validity.
	if(!_resource) return;
	// Emitting a signal in order to remove the resource from project.
	emit openFileRequest(_resource->path(), _resource->type());
}

/*
	ResourceWidget::contextMenuEvent()

	Handles the context menu event of the widget. This menu mainly allows
	to move the resource to another category or remove it from project.

	@arg	event	Context menu event structure.
*/

void ResourceWidget::contextMenuEvent(QContextMenuEvent *event)
{
	// Using a context menu to show.
	QMenu menu;
	// Using an action to retrieve the menu's result.
	QAction *result = NULL;

	// Using menu-children actions.
	QAction *action_remove = new QAction(Icon::Remove, "Remove from "
		"project", &menu);

	// Adding the actions.
	menu.addAction(action_remove);

	// Executing the context menu.
	result = menu.exec(event->globalPos());

	// The widget needs update, otherwise the light background may remain
	// after the menu was closed, if the cursor has moved outside it.
	// But, the action "Remove from project" will certainly lead to widget
	// destruction (refreshing project view resource list).
	// In other words, there is nothing that can be done after execution
	// of slot "remove". That's why slot execution is handled by taking
	// advantage of the exec() method return value -- triggered action.

	// Handling remove request.
	if(result == action_remove)
	{
		// Calling the remove slot.
		remove();
		// Returning from function because the widget has probably
		// been destroyed.
		return;
	}

	// Updating the widget to erase the light background if the mouse has
	// moved out of the widget.
	update();
}
