/*
	Header inclusions.
*/

// Qt standard headers.
#include <QApplication>
#include <QColor>
#include <QFile>
#include <QFontDatabase>
#include <QString>

// Custom headers.
#include "Icon.hpp"
#include "MainWindow.hpp"
#include "StyleParser.hpp"



/*
	main()

	Program main function. Handles global resources despite many features
	are delayed to main classes.

	@param	argc	Number of command-line arguments.
	@param	argv	NULL-terminated pointer array of command-line arguments.
	@return		Error code.
*/

int main(int argc, char **argv)
{
	// Creating the application object.
	QApplication app(argc,argv);
	// Initializing icon database.
	Icon::init("iconset.png", QColor(0xb2,0xb7,0xb4), 6);
	// Creating the main window.
	MainWindow window;

	// Loading the program style sheet.
	StyleSheet style = StyleParser::parse("fxSDK.style");
	app.setStyleSheet(style.theme("dark"));

	// Setting up the font database.
//	QFontDatabase::addApplicationFont("Consolas.ttf");

	// Showing the main window.
	window.show();
	// Running the Qt application.
	return app.exec();
}



const char *str(QString str)
{
	return str.toUtf8().constData();
}

/*

QTimer::singleShot(ms_delay, receiver, SLOT(...));

Logo by `webdesignhot`
http://www.webdesignhot.com/

*/
