#include "DynamicMenuBar.hpp"

DynamicMenuBar::DynamicMenuBar(QWidget *parent)
	: QObject(parent)
{
	menus = QList<struct Menu>();
	currentMenuBar = new QMenuBar(parent);
}

DynamicMenuBar::~DynamicMenuBar(void)
{
	if(currentMenuBar) delete currentMenuBar;
}

void DynamicMenuBar::addMenu(DynamicMenu *menu, QString name)
{
	struct Menu m;
	m.pointer = menu;
	m.name = name;

	menus << m;
}

DynamicMenu *DynamicMenuBar::newMenu(QString name)
{
	DynamicMenu *menu = new DynamicMenu(this);
	addMenu(menu,name);
	return menu;
}

void DynamicMenuBar::prepareMerge(DynamicMenu *menu/*, MergingBehavior behavior*/)
{
	if(!menu) return;
	struct MergeOperation op;
	op.pointer = menu;
//	op.behavior = behavior;
	operation << op;
}

QMenuBar *DynamicMenuBar::merge(void)
{
	QList<struct DynamicMenu::Element> dyna;
	QList<struct DynamicMenu::Element> els;
	int i,j,k;
	QMenu *m;

	for(i=0;i<operation.count();i++) if(operation[i].pointer)
		dyna << (operation[i].pointer)->_elements();
	currentMenuBar->clear();

	for(i=0;i<menus.count();i++)
	{
		m = currentMenuBar->addMenu(menus[i].name);
		els = menus[i].pointer->_elements();

		for(j=0;j<els.count();j++)
		{
			if(els[j].type == DynamicMenu::Category)
			{
				for(k=0;k<dyna.count();k++) if(dyna[k].category == els[j].categoryId)
					addToMenu(m,dyna[k]);
			}
			else addToMenu(m,els[j]);
		}

		m->menuAction()->setVisible(!m->isEmpty());
	}

	operation.clear();
	return currentMenuBar;
}

QMenuBar *DynamicMenuBar::merge(DynamicMenu *menu)
{
	prepareMerge(menu);
	return merge();
}

void DynamicMenuBar::addToMenu(QMenu *menu, DynamicMenu::Element el)
{
	switch(el.type)
	{
		case DynamicMenu::Action:
			menu->addAction(el.action);
			break;

		case DynamicMenu::Separator:
			menu->addSeparator();
			break;

		case DynamicMenu::Menu:
			menu->addMenu(el.menu);
			break;

		default:
			break;
	}
}
