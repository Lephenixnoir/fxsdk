/*
	Header inclusions.
*/

#include "ProjectManager.hpp"



/*
	ProjectManager class definition.
*/

/*
	ProjectManager::ProjectManager()

	Constructs a ProjectManager object. This class is meant to be used only
	once, by the main window.
	This object is the interface between the main window project-related
	user controls and the project.
	It completely controls the project view and the project object itself.
	It manages which project is currently opened, creates, allocates and
	frees projects, and handles user interface for all project-related
	functions that need one, including external processes.

	@arg	parent	Parent object.
*/

ProjectManager::ProjectManager(QObject *parent)
	: QObject(parent), _current(0), process(0)
{
}

/*
	ProjectManager::~ProjectManager()

	Destroys a ProjectManager object.
*/

ProjectManager::~ProjectManager(void)
{
	if(_current) delete _current;
	if(process) delete process;
}

Project *ProjectManager::currentProject(void)
{
	return _current;
}

void ProjectManager::cleanTemporaryProject(void)
{
	system("rm -rf tmp");
	system("cp -r default tmp");
}

void ProjectManager::newProject(void)
{
	if(_current) _current->save();
	delete _current;

	cleanTemporaryProject();

	_current = new Project();
	_current->addResource("tmp/main.c");
	_current->addResource("tmp/crt0.s");
	_current->addResource("tmp/libfx.a");

	emit(changed(_current));
}

void ProjectManager::openProject(void)
{
	Project *project;
	QString file;

	QFileDialog d;
	d.setWindowTitle("Open project file...");
	d.setFileMode(QFileDialog::ExistingFile);
	d.setAcceptMode(QFileDialog::AcceptOpen);
	d.exec();
	if(!d.result()) return;

	file = d.selectedFiles()[0];

	project = new Project(file);

	delete _current;
	_current = project;

	emit(changed(_current));
}

void ProjectManager::addFile(QString file)
{
	if(!_current || file.isEmpty()) return;

	_current->addResource(file);
	_current->save();

	emit changed(_current);
}

void ProjectManager::addFiles(void)
{
	int i;
	QStringList files;
	if(!_current) return;

	QFileDialog d;
	d.setWindowTitle("Add files to project...");
	d.setFileMode(QFileDialog::ExistingFiles);
	d.setAcceptMode(QFileDialog::AcceptOpen);
	d.exec();
	if(!d.result()) return;

	files = d.selectedFiles();

	for(i=0; i<files.count(); i++) _current->addResource(files[i]);
	_current->save();

	emit changed(_current);
}

void ProjectManager::rebuildAll(void)
{
	QStringList args;
	if(!_current) return;

	args << "mrproper" << "-C" << _current->path();

	process = new QProcess(this);
	connect(process, SIGNAL(readyReadStandardOutput(void)), this,
		SLOT(buildOutput(void)));
	connect(process, SIGNAL(readyReadStandardError(void)), this,
		SLOT(buildOutput(void)));
	connect(process, SIGNAL(finished(int, QProcess::ExitStatus)),
		this, SLOT(build(void)));

	process->start("make", args);
}

void ProjectManager::build(void)
{
	QStringList arguments;
	if(!_current) return;

	_current->makefile();
	arguments << "-C" << _current->path();

	process = new QProcess(this);
	connect(process, SIGNAL(readyReadStandardOutput(void)), this,
		SLOT(buildOutput(void)));
	connect(process, SIGNAL(readyReadStandardError(void)), this,
		SLOT(buildOutput(void)));

	process->start("make", arguments);
}

void ProjectManager::buildOutput(void)
{
	if(!process) return;

	QString output = QString(process->readAllStandardOutput());
	QString error = QString(process->readAllStandardError());
	QString text = error + output;

	if(text.isEmpty()) return;
	if(text.right(1) == "\n") text = text.left(text.length() - 1);

	emit(buildOutputAvailable(text));
}
