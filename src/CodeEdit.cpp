#include "CodeEdit.hpp"

CodeEdit::CodeEdit(QWidget *parent, QString language_file)
	: QPlainTextEdit(parent), highlighter(0)
{
	setWordWrapMode(QTextOption::NoWrap);

	changeSyntaxHighlighter(language_file);

	QPalette p = palette();
	p.setColor(QPalette::Highlight,QColor(0x42,0x47,0x44));
	setPalette(p);

	lines = new LinesArea(this);
	setPlainText("");

	setWindowModified(false);
	updateLinesAreaWidth();
//	highlightCurrentLine();

	connect(this,SIGNAL(blockCountChanged(int)),this,SLOT(updateLinesAreaWidth(int)));
	connect(this,SIGNAL(updateRequest(QRect,int)),this,SLOT(updateLineNumberArea(QRect,int)));
//	connect(this,SIGNAL(cursorPositionChanged()),this,SLOT(highlightCurrentLine()));
}

void CodeEdit::linesPaintEvent(QPaintEvent *event)
{
	QPainter painter(lines);
	painter.setFont(font());

	QTextBlock block = firstVisibleBlock();
	int blockNumber = block.blockNumber();
	int top = (int)blockBoundingGeometry(block).translated(contentOffset()).top();
	int bottom = top + (int)blockBoundingRect(block).height();

	while (block.isValid() && top <= event->rect().bottom())
	{
		if (block.isVisible() && bottom >= event->rect().top())
		{
			QString number = QString::number(blockNumber + 1);
			painter.setPen(QColor(0x40, 0x40, 0x40));
			painter.drawText(2, top, lines->width()-4, fontMetrics().height()+2, Qt::AlignRight | Qt::AlignVCenter, number);
		}

		block = block.next();
		top = bottom;
		bottom = top + (int)blockBoundingRect(block).height();
		++blockNumber;
	}
}

void CodeEdit::changeSyntaxHighlighter(QString language_file)
{
	if(highlighter) delete highlighter;
	if(language_file.isEmpty()) return;

	new Highlighter(this->document(), language_file);
}

void CodeEdit::resizeEvent(QResizeEvent *e)
{
	QPlainTextEdit::resizeEvent(e);

	QRect r = contentsRect();
	lines->setGeometry(QRect(r.left(),r.top(),linesAreaWidth(),r.height()));
}

void CodeEdit::updateLinesAreaWidth(int)
{
	setViewportMargins(linesAreaWidth() + 8, 0, 0, 0);
}

void CodeEdit::highlightCurrentLine()
{
	QList<QTextEdit::ExtraSelection> extraSelections;

	if(!isReadOnly()) {
		QTextEdit::ExtraSelection selection;

		QColor lineColor = QColor(0x22,0x27,0x24).lighter(120);

		selection.format.setBackground(lineColor);
		selection.format.setProperty(QTextFormat::FullWidthSelection, true);
		selection.cursor = textCursor();
		selection.cursor.clearSelection();
		extraSelections.append(selection);
	}

	setExtraSelections(extraSelections);
}

void CodeEdit::updateLineNumberArea(QRect r, int dy)
{
	if(dy) lines->scroll(0,dy);
	else lines->update(0,r.y(),lines->width(),r.height());

	if(r.contains(viewport()->rect())) updateLinesAreaWidth();
}

int CodeEdit::linesAreaWidth()
{
	int digits = 0;
	int lines = blockCount();
	if(!lines) lines = 1;

	while(lines) digits++, lines /= 10;

	return digits * fontMetrics().width(QLatin1Char('9')) + 8;
}





LinesArea::LinesArea(CodeEdit *p_editor)
	: QWidget(p_editor)
{
	editor = p_editor;
	setFocusPolicy(Qt::NoFocus);
}

void LinesArea::paintEvent(QPaintEvent *event)
{
	if(editor) editor->linesPaintEvent(event);
}
