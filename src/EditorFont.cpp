#include "EditorFont.hpp"

EditorFont::EditorFont(QWidget *parent, QString resource)
	: EditorWidget(parent, resource)
{
}

void EditorFont::setChanged(bool hasChanged)
{
	EditorWidget::setChanged(hasChanged);
}

void EditorFont::save(bool saveas)
{
}

void EditorFont::saveAs(void)
{
	save(true);
}
