/*
	Header inclusions.
*/

#include "EditorSet.hpp"



/*
	EditorSet::EditorSet()

	Constructs an editor set. An editor set is a set of editor widgets and
	classical widgets. It also allows to create custom dynamic menu bar
	elements, that change on focus.
	The editor set appearance is not fixed. For now, it can only be
	displayed as a tab widget but the editor selector may have different
	forms.

	@warn	The editor set takes ownership of the editors and destroys them
		when their associated tabs are closed.

	@arg	parent	The parent widget.
*/

EditorSet::EditorSet(QWidget *parent)
	: QWidget(parent), w(100), h(24), active(-1)
{
	// Initializing the editor list.
	elements = QList<struct Element>();

	// Creating the arrow selectors, that allow to switch between widgets.
	arrowl = new EditorSetArrow(EditorSetArrow::Left, this);
	arrowr = new EditorSetArrow(EditorSetArrow::Right, this);

	// Connecting their signals to the corresponding slot.
	connect(arrowl, SIGNAL(clicked(bool)), this, SLOT(switchEditor(void)));
	connect(arrowr, SIGNAL(clicked(bool)), this, SLOT(switchEditor(void)));

	// Creating the menus and actions.
	QAction *close_current = new QAction(this);
	close_current->setShortcut(Qt::CTRL | Qt::Key_W);
	addAction(close_current);
	connect(close_current, SIGNAL(triggered(void)), this,
		SLOT(closeCurrentEditor(void)));


	// Creating the dynamic menu.
	menu = new DynamicMenu(this);

	// Creating an action to add the current file to the project.
	action_add = menu->newAction(Icon::NewProject, "Add current file to "
		"project", "Ctrl+J", this, DynamicCategory::Project);
	action_add->setShortcutContext(Qt::WidgetWithChildrenShortcut);
	connect(action_add, SIGNAL(triggered(void)), this,
		SLOT(addCurrentFile(void)));
	// Hiding the actions.
	action_add->setVisible(false);
}

/*
	EditorSet::~EditorSet()

	Destroys the editor set.
*/

EditorSet::~EditorSet(void)
{
	int count;
	setActive(0, false);

	while((count = elements.count()))
	{
		closeCurrentEditor();
		if(elements.count() == count) setActive(active+1,false);
	}
}

/*
	EditorSet::addWidget()

	Adds a widget to the editor set. Also selects it by default.

	@arg	widget	The widget.
	@arg	name	The widget name.

	@return	The widget given as argument.
*/

QWidget *EditorSet::addWidget(QWidget *widget, QString name)
{
	// Taking ownership of the widget.
	widget->setParent(this);
	// Resizing and moving it to fit the editor set.
	widget->setGeometry(0,h+4,width(),height()-(h+4));

	// Creating an element structure based on the editor.
	struct Element el;
	el.widget = widget;
	el.isEditor = false;
	el.name = name;

	// Adding structure and selecting the widget.
	elements << el;
	setActive(elements.count()-1);

	// Updating editor set view.
	update();

	// Returning the added editor.
	return widget;
}

/*
	EditorSet::addEditor()

	Adds an editor to the editor set. Also selects it by default.

	@arg	editor	The editor widget.
	@arg	name	The editor name.

	@return			The widget given as argument.
*/

EditorWidget *EditorSet::addEditor(EditorWidget *editor, QString name)
{
	// Taking ownership of the widget.
	editor->setParent(this);
	// Resizing and moving it to fit the editor set.
	editor->setGeometry(0, h+4, width(), height() - (h+4));

	// Creating an element structure based on the editor.
	struct Element el;
	el.editor = editor;
	el.isEditor = true;
	// Setting editor name : file name if exists, given argument otherwise.
	QString path = editor->file();
	el.name = path.isEmpty() ? name : QFileInfo(path).fileName();

	// Adding structure and selecting the editor.
	elements << el;
	setActive(elements.count() - 1);

	// Connecting editor's nameChanged() signal.
	connect(editor, SIGNAL(nameChanged(QString)), this,
		SLOT(updateEditorName(QString)));

	// Updating editor set view.
	update();

	// Returning the added editor.
	return editor;
}

/*
	EditorSet::setActive()

	Selects the given editor (specified by position in editor list) and
	updates the view.

	@arg	editor_num	The editor position in the editor list.
	@arg	previous	Specifies if the previous widget still exists.
				If set to false, the previous widget is not
				hidden (use if you destroyed the current widget
				and want to select another). Defaults true.
*/

void EditorSet::setActive(int n, bool previous)
{
	// Using a dynamic menu pointer.
	DynamicMenu *menu;

	// Using no selection if the editor list is empty.
	if(n < 0 && !elements.count())
	{
		// Assigning a special value to active.
		active = -1;
		// Emitting the focus signal.
		emit focused(0);
		// Updating the actions and the view.
		updateActions();
		update();
		// Returning from function. 
		return;
	}
	// Avoiding selecting non-existing elements.
	if(n < 0 || n >= elements.count()) return;

	// Hiding the previous widget.
	if(previous && active != -1) elements[active].widget->hide();
	// Setting `active` and showing the associated widget.
	active = n;
	elements[active].widget->show();
	// Giving the focus.
	elements[active].widget->setFocus(Qt::OtherFocusReason);


	// Setting actions visibility.
	updateActions();

	// Getting the new dynamic menu.
	menu = dynamicMenuFor(elements[active].widget);
	// Updating the dynamic menu bar.
	emit focused(menu);

	// Updating the view.
	update();
}

/*
	EditorSet::dynamicMenu()

	Returns the editor set dynamic menu.

	@return		Dynamic menu of editor set.
*/

DynamicMenu *EditorSet::dynamicMenu(void)
{
	return menu;
}

/*
	EditorSet::dynamicMenuFor()

	Returns the dynamic menu of any element defined by its widget pointer.

	@arg	pointer		Address of element.

	@return		Object's dynamic menu.
*/

DynamicMenu *EditorSet::dynamicMenuFor(QObject *pointer)
{
	// Avoiding incorrect element selection.
	if(!pointer || pointer==arrowl || pointer==arrowr) return 0;
	// Using an incremental counter.
	int i;

	for(i=0; i < elements.count(); i++)
	{
		if(elements[i].isEditor && elements[i].editor == pointer)
			return elements[i].editor->dynamicMenu();
	}

	return 0;
}

/*
	EditorSet::isOpened()

	Checks if a file is opened if an editor. The corresponding editor gets
	focus.

	@arg	path	File path.

	@return		true if the file is opened somewhere, false otherwise.
*/

bool EditorSet::isOpened(QString path)
{
	// Using an incremental counter.
	int i;

	for(i=0; i<elements.count(); i++)	
	{
		if(elements[i].isEditor && elements[i].editor->file() == path)
		{
			setActive(i);
			return true;
		}
	}

	return false;
}

/*
	EditorSet::closeCurrentEditor()

	Closes the current editor.
*/

void EditorSet::closeCurrentEditor(void)
{
	// Preventing the function from closing non-existing elements.
	if(!elements.count() || active == -1) return;

	// Displaying a message box if the opened file has been edited.
	if(elements[active].isEditor && elements[active].editor->changed())
	{
		// Using a QMessageBox.
		QMessageBox b;

		// Setting its text.
		b.setText("File <b>" + elements[active].name + "</b> has been"
			" edited but not saved.");
		b.setInformativeText("What do you want to do ?");
		// Setting standard buttons and selecting default one.
		b.setStandardButtons(QMessageBox::Save | QMessageBox::Discard
			| QMessageBox::Cancel);
		b.setDefaultButton(QMessageBox::Save);

		// Executing the message box and retrieving result.
		switch(b.exec())
		{
			// Canceling the operation makes the function return.
			case QMessageBox::Cancel:
				return;
			// Calling the editor saving function if wanted.
			case QMessageBox::Save:
				elements[active].editor->save();
			// Closing without saving doesn't do anything.
			// Using default handles any other StandardButtons.
			default:
				break;
		}
	}

	// Determining what editor should be selected next. This value takes in
	// consideration that the current editor will be deleted.
	int next_active;
	// If the current editor is the last one, cancel focus.
	if(elements.count() == 1) next_active = -1;
	// If the current editor is the last, select the previous.
	else if(active == elements.count() - 1) next_active = active - 1;
	// In any other case, select the one that will replace it.
	else next_active = active;

	// Deleting the widget. Removing the list entry.
	delete elements[active].widget;
	elements.removeAt(active);
	// Selecting another editor.
	setActive(next_active, false);

	// Updating the view.
	update();
}

/*
	EditorSet::updateEditorName()

	Updates the name of an editor, for example when a file has been saved
	under a new name.

	@arg	new_name	New name for the editor.
*/

void EditorSet::updateEditorName(QString new_name)
{
	// Retrieving sender address.
	QObject *sender = QObject::sender();

	// Iterating over the elements array. If an editor is added several
	// times, all names are changed.
	for(int i=0;i<elements.count();i++)
	{
		// Matching the editors that sent the signal.
		if(elements[i].isEditor && elements[i].editor == sender)
		{
			// Setting their names.
			elements[i].name = new_name;
		}
	}

	// When a file name is changed, this slot is called. This is tricky but
	// we will also update actions visibility here (we should connect a
	// slot that would be called when the file name is changed, but since
	// this one is supposed to be called, it's fine).
	updateActions();

	// Re-drawing widget.
	update();
}

/*
	EditorSet::addCurrentFile()

	Emits a request to add the current file to the project.
*/

void EditorSet::addCurrentFile(void)
{
	// Checking that there's a current tab opened.
	if(!elements.count() && active >= 0) return;

	// Checking that the current tab is an editor.
	if(!elements[active].isEditor) return;

	// Checking that the edited file is saved on disk.
	if(elements[active].editor->file().isEmpty()) return;

	// Emitting a request.
	emit addToProjectRequest(elements[active].editor->file());
}

/*
	EditorSet::resizeEvent()

	Overloaded event handler. Resizes the contained widgets to the new
	width and height of the editor set, and moves the arrows.

	@arg	[anonymous]	Event structure.
*/

void EditorSet::resizeEvent(QResizeEvent *)
{
	// Using an iterator.
	int i;

	// Resizing the widgets.
	for(i=0; i < elements.count(); i++)
	{
		// Setting the geometry of the current element.
		elements[i].widget->setGeometry(0, h+4, width(),
			height() - (h + 4));
	}

	// Moving the arrows.
	arrowl->move(width()-32,14 - (arrowl->height() >> 1));
	arrowr->move(width()-18,14 - (arrowr->height() >> 1));
}

/*
	EditorSet::paintEvent()

	Overloaded event handler. Paints the widget. This handler is called
	before the children are painted, if shown. Therefore, the arrows and
	the currently shown widget are drawn over the event handler drawing.

	@arg	[anonymous]	Event structure.
*/

void EditorSet::paintEvent(QPaintEvent *)
{
	// This boolean indicates whether there are editors opened, or not.
	bool has_editors = elements.count() != 0;

	// Hiding the arrows if there is no editors, showing them otherwise.
	arrowl->setVisible(has_editors);
	arrowr->setVisible(has_editors);

	// Creating a painter object.
	QPainter p(this);

	// If there is no editor, drawing a special background and returning.
	if(!has_editors)
	{
		// Getting a pixmap.
//		QPixmap px("logo.png");
		// Drawing it at the center of screen.
//		p.drawPixmap((width()-px.width()) >> 1,
//			(height()-px.height()) >> 1,px);
		// Returning from the function.
		return;
	}

	// Incremental counter declaration.
	int i;
	// Filling the tab bar with the background. The widget background is
	// already filled due to style sheets declarations.
	p.fillRect(0,0,width()-1,h+3,QColor(0x1a,0x1f,0x1b));

	// Saving painter parameters.
	p.save();
	// Reducing the painting area to avoid drawing over widgets.
	p.setClipRect(0,4,width()-40,h);
	// Rendering the tabs left and right to the current tab, in the right
	// order to make the tab flow natural.
	for(i=0;i<active;i++) renderTab(&p,i,0);
	for(i=elements.count()-1;i>active;i--) renderTab(&p,i,0);

	// Extending the painting area to full width to draw the line.
	p.setClipRect(0,4,width(),h);
	// De-activating anti-aliasing to get a thin, clear line.
	p.setRenderHint(QPainter::Antialiasing,false);
	// Setting line color.
	p.setPen(QColor(0x17,0x17,0x12));
	// Drawing the baseline at the very bottom, over the tabs.
	p.drawLine(0,h+3,width()-1,h+3);

	// Rendering the current tab over the line.
	renderTab(&p,active,1);
	// Restoring painter parameters for possible additional drawing.
	p.restore();
}

/*
	EditorSet::mousePressEvent()

	Overloaded event handler. Selects the corresponding tab if needed.

	@arg	event	Event structure.
*/

void EditorSet::mousePressEvent(QMouseEvent *event)
{
	// Getting event coordinates.
	const int x = event->x(), y = event->y();
	// Testing if the event takes place on the tab bar.
	if(y < 4 || y > h+4 || x < 0 || x > width()-40) return;
	// Setting the active tab.
	setActive(tab_at(x));
}

/*
	EditorSet::keyPressEvent()

	Handles Ctrl + PageUp and Ctrl + PageDown to switch between editors.

	@arg	event	Event structure.
*/

void EditorSet::keyPressEvent(QKeyEvent *event)
{
	// Eliminating events without a [Ctrl] key press.
	if(event->modifiers() != Qt::ControlModifier) return;

	// Handling PageUp key event.
	if(event->key() == Qt::Key_PageUp) switchEditor(-1);
	// Handling PageDown key event.
	else if(event->key() == Qt::Key_PageDown) switchEditor(1);
}

/*
	EditorSet::switchEditor()

	Selects a different editor by adding a displacement to the current
	selected id.

	@warn	Will only work with values greater than or equal to
		-(editor count).

	@arg	disp	The value to add to the active editor identifier.
*/

void EditorSet::switchEditor(int disp)
{
	if(disp)
	{
		setActive((active + disp + elements.count()) % elements.count());
		return;
	}

	QObject *sender = QObject::sender();
	if(sender == arrowl) setActive((active-1+elements.count()) % elements.count());
	else if(sender == arrowr) setActive((active+1) % elements.count());
}

/*
	EditorSet::tabs_overflow()

	Returns false is the editor widget is large enough for all tabs.
	Otherwise, returns true.

	@return		A boolean indicating if there is enough space for all
			tabs. Returns true if the tabs overflow the widget.
*/

bool EditorSet::tabs_overflow(void)
{
	// Computing the total tab length and comparing it to the widget width.
	return elements.count() * (w+16) + 8 > width() - 40;
}

/*
	EditorSet::tab_at()

	Returns the id of the widget at the given x-coordinate, or -1 if there
	is no tab at the specified position.

	@arg	x	Coordinate of the point which has to be tested.
	@return		The id of the tab.
*/

int EditorSet::tab_at(int x)
{
	// If tabs don't overflow, operation if far simpler.
	if(!tabs_overflow())
	{
		// Dividing `x` by the length of a tab.
		x /= (w+16);
		// Testing if the computed isn't out of the tabs bar.
		return x >= elements.count() ? -1 : x;
	}

	// Computing the width of the "drawing section". It represents the length between the
	// positions where the first and the last tabs are drawn (ignoring the length of
	// the rightmost tab).
	const qreal tw = width()-40-(w+24);
	// Computing the length of a tab.
	const qreal width = tw / (elements.count()-1);
	// Adjusting x, to take in consideration that the active tab is drawn completely, and
	// therefore longer than the others.
	if(x/width > active+1) x -= (w+24) - width;
	// Dividing x by the length of a tab to get the id.
	x /= width;
	// Adjusting x makes as if every tab had the same length (including the selected tab).
	// Therefore, the additional length of the selected tab is "moved" at the right of the
	// drawing section and this little space will return an id of elements.count().
	if(x >= elements.count()) x = elements.count() - 1;
	// Returning the computed tab id.
	return x;
}

/*
	EditorSet::renderTab()

	Renders the given tab (specified by identifier) using the given
	painter. This function computes the position without subroutine.

	@arg	p	The painter to use.
	@arg	i	The id of the tab.
*/

void EditorSet::renderTab(QPainter *p, int i, int active)
{
	// Computing the tab bar height.
	const qreal height = h + 4;
	// Computing the length of the rendering section in the case the tabs
	// overflow.
	const qreal tw = width()-40-(w+24);
	// Computing the position where the tab should be drawn.
	const qreal x = tabs_overflow() ? i * tw / (elements.count()-1)
		: i * (w+16);
	// Using an array to simplify the expression of three important
	// heights : tab top, height where the rounding curve begins, and tab
	// base.
	qreal l[3] = { height - h + 0.5, height - h + 8, height };

	// Creating a path that describes the border of the tab.
	QPainterPath path;
	path.moveTo(x,l[2]);
	path.lineTo(x+6,l[1]);
	path.quadTo(x+8,l[0],x+12,l[0]);
	path.lineTo(x+w+12,l[0]);
	path.quadTo(x+w+16,l[0],x+w+18,l[1]);
	path.lineTo(x+w+24,l[2]);

	// Creating a linear gradient as background for the tab.
	QLinearGradient g(0,4,0,28);
	if(active)
	{
		g.setColorAt(0,QColor(0x55,0x59,0x56));
		g.setColorAt(0.05,QColor(0x55,0x59,0x56));
		g.setColorAt(0.1,QColor(0x2d,0x31,0x2e));
	}
	else
		g.setColorAt(0,QColor(0x2d,0x31,0x2e));
	g.setColorAt(1.0,QColor(0x25,0x29,0x26));

	// Setting anti-aliasing.
	p->setRenderHint(QPainter::Antialiasing,true);

	// Setting the painter's pen and brush.
	p->setPen(QColor(0x13,0x17,0x12));
	p->setBrush(g);
	// Drawing the path, including the background/
	p->drawPath(path);

	// Setting a light pen for the editor name.
	p->setPen(QColor(0xbd,0xc0,0xc2,180));
	// Saving the parameters.
	p->save();
	// Setting a little font size for non-selected tabs.
	if(!active)
	{
		QFont f = p->font();
		int ps = f.pointSize();

		f.setPointSize(ps > 0 ? ps * 0.85 : 5.5);
		p->setFont(f);
	}
	// Drawing the editor name centered on the tab.
	p->drawText(x +12, 4, w, 24 ,Qt::AlignCenter, elements[i].name);
	// Restoring the initial parameters.
	p->restore();
}

/*
	EditorSet::updateActions()

	Toggles the actions visibility depending on the current parameters.
*/

void EditorSet::updateActions(void)
{
	// Hiding them all, showing them after if needed.
	action_add->setVisible(false);

	// Handling action 'Add current file to project'.
	if(elements.count() && active >= 0 && elements[active].isEditor)
	{
		if(!elements[active].editor->file().isEmpty())
			action_add->setVisible(true);
	}
}

/*
	EditorSet::EditorSetArrow::EditorSetArrow()

	Constructs a button that has the appearance of an oriented arrow
	(triangle) and highlights when hovered. Originally used for its auto-
	repeat properties although the current implementation doesn't use them.

	@arg	o	The arrow orientation. See
			EditorSet::EditorSetArrow::Orientation enumeration.
	@arg	parent	The parent widget. As every widget, the button is
			automatically added to the widgets of the parent since
			the parent class is constructed using this argument.
*/

EditorSet::EditorSetArrow::EditorSetArrow(Orientation _o, QWidget *parent)
	: QAbstractButton(parent), o(_o), hovered(false)
{
	// Setting arrow size.
	setFixedSize(10,12);

	/*
	// Setting auto-repeat parameters.
	setAutoRepeat(true);
	setAutoRepeatDelay(500);
	setAutoRepeatInterval(100);
	*/
}

/*
	EditorSet::EditorSetArrow::paintEvent()

	Overloaded event handler. Draws the arrow depending on the selected
	orientation.

	@arg	[anonymous]	Event structure.
*/

void EditorSet::EditorSetArrow::paintEvent(QPaintEvent *)
{
	// Creating the point lists.
	QPoint pl[3] = { QPoint(2,6), QPoint(10,1), QPoint(10,11) };
	QPoint pr[3] = { QPoint(8,6), QPoint(0,1), QPoint(0,11) };
	// Creating the colors, for normal and hover states.
	QColor cn(0x4c,0x4c,0x4c);
	QColor ch(0x6c,0x6c,0x6c);

	// Creating the painter object.
	QPainter p(this);
	// Enabling anti-aliasing.
	p.setRenderHint(QPainter::Antialiasing,true);
	// Setting the pen and brush.
	p.setPen(hovered ? ch : cn);
	p.setBrush(hovered ? ch : cn);

	// Drawing the polygon associated width the arrow orientation.
	if(o == Left) p.drawPolygon(pl,3);
	else p.drawPolygon(pr,3);
}

/*
	EditorSet::EditorSetArrow::enterEvent()

	Overloaded event handler. Sets the hover state for the button.

	@arg	[anonymous]	Event structure.
*/

void EditorSet::EditorSetArrow::enterEvent(QEvent *)
{
	// Setting the hover state.
	hovered = true;
}

/*
	EditorSet::EditorSetArrow::leaveEvent()

	Overloaded event handler. Removes the hover state for the button.

	@arg	[anonymous]	Event structure.
*/

void EditorSet::EditorSetArrow::leaveEvent(QEvent *)
{
	// De-activating the hover state.
	hovered = false;
}
