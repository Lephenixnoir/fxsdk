#include "MainWindow.hpp"

/*
	MainWindow::MainWindow()

	Constructs a MainWindow object. Requires no parameter since it does
	does only create a static window with no customizable options.
*/

MainWindow::MainWindow(void)
	: dynamicMenu(0)
{
	// Creating the project manager (needs to be done before setting up the menu bar).
	projectManager = new ProjectManager(this);
	// Creating the project view.
	projectView = new ProjectView(this);
	// Actually the dock isn't movable due to custom title bar...
	projectView->setAllowedAreas(Qt::LeftDockWidgetArea);
	// Ensuring a minimum size.
	projectView->setMinimumSize(120,1);
	// Adding the project dock to the main window.
	addDockWidget(Qt::LeftDockWidgetArea,projectView);

	// Setting up the menu bar.
	setupMenuBar();

	// Creating a second dock widget thought to receive compiler output.
	DockWidget *dock2 = new DockWidget("Compiler output", Qt::Vertical,
		this);
	// Configuring it.
	dock2->setAllowedAreas(Qt::BottomDockWidgetArea);
	dock2->setMinimumSize(1,80);
	// Adding the text editor.
	output_edit = new QPlainTextEdit(this);
	output_edit->setReadOnly(true);
	dock2->addWidget(output_edit);
	// Adding it to the window.
	addDockWidget(Qt::BottomDockWidgetArea,dock2);

	// Ensuring that the project panel will look at a higher level by spanning all height.
	setCorner(Qt::BottomLeftCorner,Qt::LeftDockWidgetArea);

	// Creating the editor widget.
	editors = new EditorSet(this);
	// Setting it as central widget for the window.
	setCentralWidget(editors);

	// Setting minimum size for the window.
	setMinimumSize(640, 480);

	// Setting up the connection to make effective the adaptive menu system.
	connect(editors, SIGNAL(focused(DynamicMenu *)), this,
		SLOT(setDynamicMenu(DynamicMenu *)));
	// Adding the current editor files to project.
	connect(editors, SIGNAL(addToProjectRequest(QString)), projectManager,
		SLOT(addFile(QString)));
	// Changes in project may lead to dynamic menu updates.
	connect(projectManager, SIGNAL(changed(Project *)), this,
		SLOT(updateMenuBar()));
	// Changes in the project need to update the view.
	connect(projectManager, SIGNAL(changed(Project *)), projectView,
		SLOT(updateView(Project *)));
	// Appends the build output to the text area.
	connect(projectManager, SIGNAL(buildOutputAvailable(QString)), this,
		SLOT(outputBuildAppend(QString)));

	connect(projectView, SIGNAL(fileOpenRequest(QString,
		enum Resource::Type)), this, SLOT(project_fileOpenRequest(
		QString, enum Resource::Type)));

	// Creating a project to begin working.
	projectManager->newProject();
}

/**
 *	MainWindow::file_new()
 *	@descr	Creates a new file and opens it in a new editor. For now, only creates source
 *			files. Should evolve later into a dialog-based simple interface oriented
 *			in creating different kind of resources.
 */

void MainWindow::file_new(void)
{
	// Creating a new tab with an empty resource path.
	editors->addEditor(new EditorText(this, ""), "Code editor");
}

/**
 *	MainWindow::file_open(void)
 *	@descr	Opens the selected files in new editors. For now, always loads files as source
 *			code resources. Should be adapted to work with different resource types
 *			(probably using class-independent file recognition implementations).
 */

void MainWindow::file_open(void)
{
	// Creating a QFileDialog.
	QFileDialog d;
	// Configuring it for opening any files in reading mode.
	d.setFileMode(QFileDialog::ExistingFiles);
	d.setAcceptMode(QFileDialog::AcceptOpen);
	d.setNameFilter("All files (*)");
	// Waiting for user input.
	d.exec();
	// Checking input cancellation.
	if(!d.result()) return;

	// Getting the selected files paths.
	QStringList list = d.selectedFiles();
	// Opening each file in a new editor.
	for(int i=0;i<list.count();i++)
		editors->addEditor(new EditorText(this,list[i]),"Code editor");
}

/**
 *	MainWindow::view_menu()
 *	@descr	Toggles the menu bar visibility.
 */

void MainWindow::view_menu(void)
{
	// Toggling menu bar visibility.
	menuBar()->setVisible(!menuBar()->isVisible());
}

/**
 *	MainWindow::view_fullscreen()
 *	@descr	Toggles the full screen mode.
 */

void MainWindow::view_fullscreen(void)
{
	// Toggling full screen.
	if(isFullScreen()) showNormal();
	else showFullScreen();
}

/**
 *	MainWindow::updateMenuBar()
 *	@descr	Updates the menu bar with the focused widget custom menu bar elements.
 *			Intended to insert an `Edit` menu at the second position in the menu bar.
 *	@warn	Doesn't actually work since the focused widget is the widget contained in the
 *			editor instead of the editor itself, as I want.
 *
 *	@param	[an.]	Old focused widget.
 *	@param	widget	Newly-focused widget.
 */

void MainWindow::updateMenuBar(void)
{
	// Merging with the menu.
	menubar->prepareMerge(dynamicMenu);
	// Adding the editor set menu.
	menubar->prepareMerge(editors->dynamicMenu());
	// Also merging with the project menu.
	if(projectManager->currentProject()) menubar->prepareMerge(dynamicProject);

	// Finally merging and setting as menu bar.
	setMenuBar(menubar->merge());
}

void MainWindow::setDynamicMenu(DynamicMenu *menu)
{
	dynamicMenu = menu;
	updateMenuBar();
}

void MainWindow::outputBuildEmpty(void)
{
	output_edit->setPlainText("");
}

void MainWindow::outputBuildAppend(QString str)
{
	output_edit->appendPlainText(str);
}

void MainWindow::project_fileOpenRequest(QString path,
	enum Resource::Type type)
{
	if(editors->isOpened(path)) return;
	QString name = QFileInfo(path).fileName();

	switch(type)
	{
		case Resource::Source:
		case Resource::Header:
			editors->addEditor(new EditorText(this, path), name);
			break;

		case Resource::Font:
			editors->addEditor(new EditorFont(this, path), name);
			break;

		default:
			return;
	}
}

void MainWindow::updateProjectView(void)
{
	projectView->updateView(projectManager->currentProject());
}

/**
 *	MainWindow::setupMenuBar()
 *
 *	Creates the menu bar with the default elements and shows it. Intended
 *	to be called by the constructor only -- delegation structure.
 */

void MainWindow::setupMenuBar(void)
{
	QAction *view_menu = new QAction(Icon::Menu,"Toggle menu",this);
		view_menu->setShortcut(Qt::Key_F10);
		connect(view_menu,SIGNAL(triggered()),this,SLOT(view_menu()));
	QAction *view_fullscreen = new QAction(Icon::Fullscreen,"Toggle full screen",this);
		view_fullscreen->setShortcut(Qt::Key_F11);
		connect(view_fullscreen,SIGNAL(triggered()),this,SLOT(view_fullscreen()));
	QAction *view_project = new QAction(Icon::OpenProject, "Refresh project view", this);
		view_project->setShortcut(Qt::Key_F12);
		connect(view_project, SIGNAL(triggered()), this, SLOT(updateProjectView()));

	menubar = new DynamicMenuBar(this);

	DynamicMenu *file = menubar->newMenu("File");
		QAction *file_new = file->newAction(Icon::NewFile,"New file","Ctrl+N",this);
			connect(file_new,SIGNAL(triggered()),this,SLOT(file_new()));
		QAction *file_open = file->newAction(Icon::OpenFile,"Open files...","Ctrl+O",this);
			connect(file_open,SIGNAL(triggered()),this,SLOT(file_open()));
		file->addSeparator();
		file->addCategory(DynamicCategory::Save);
		file->addSeparator();
		QAction *file_quit = file->newAction(Icon::Exit,"Quit","Ctrl+Q",this);
			connect(file_quit,SIGNAL(triggered()),this,SLOT(close()));

	DynamicMenu *edit = menubar->newMenu("Edit");
		edit->addCategory(DynamicCategory::Edit);
		edit->addSeparator();
		/*QAction *edit_preferences = */edit->newAction(Icon::Settings,
			"fxSDK Preferences...", "Shift+Ctrl+P", this);

	DynamicMenu *project = menubar->newMenu("Project");
		QAction *project_new = project->newAction(Icon::NewProject,"New project",
			"Shift+Ctrl+N", this);
			connect(project_new,SIGNAL(triggered()),projectManager,SLOT(newProject()));
		QAction *project_open = project->newAction(Icon::OpenProject,
			"Open project...", "Shift+Ctrl+O", this);
			connect(project_open, SIGNAL(triggered()),
				projectManager, SLOT(openProject()));
		project->addSeparator();
		project->addCategory(DynamicCategory::Project);

	DynamicMenu *view = menubar->newMenu("View");
		view->addAction(view_menu);
		view->addAction(view_fullscreen);
		view->addAction(view_project);

	dynamicProject = new DynamicMenu(this);
		QAction *dynamic_project_add = dynamicProject->newAction(
			Icon::NewFile, "Add files...", "", this,
			DynamicCategory::Project);
			connect(dynamic_project_add, SIGNAL(triggered()),
			projectManager, SLOT(addFiles()));
		dynamicProject->addSeparator(DynamicCategory::Project);
		QAction *dynamic_project_build = dynamicProject->newAction(
			Icon::Build, "Build", "Ctrl+B", this,
			DynamicCategory::Project);
			connect(dynamic_project_build, SIGNAL(triggered()),
			this, SLOT(outputBuildEmpty()));
			connect(dynamic_project_build, SIGNAL(triggered()),
			projectManager, SLOT(build()));
		QAction *dynamic_project_rebuildAll = dynamicProject->
			newAction(Icon::Build, "Rebuild All", "Shift+Ctrl+B",
			this, DynamicCategory::Project);
			connect(dynamic_project_rebuildAll, SIGNAL(
			triggered()), this, SLOT(outputBuildEmpty()));
			connect(dynamic_project_rebuildAll, SIGNAL(
			triggered()), projectManager, SLOT(rebuildAll()));
		QAction *dynamic_project_settings = dynamicProject->newAction(
			Icon::Settings, "Settings...", "Ctrl+P", this,
			DynamicCategory::Project);
			connect(dynamic_project_settings, SIGNAL(triggered()),
			projectView, SLOT(configure()));

	addAction(file_new);
	addAction(file_open);
	addAction(file_quit);

	addAction(project_new);
	addAction(project_open);
	addAction(dynamic_project_build);
	addAction(dynamic_project_settings);

	addAction(view_menu);
	addAction(view_fullscreen);

	setMenuBar(menubar->merge(0));
}
