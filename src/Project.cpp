/*
	Header inclusions.
*/

#include "Project.hpp"



/*
	Project class definition.
*/



/*
	Project::Project()

	Constructs a project object. It is a non-Qt class that interfaces with
	the file system, loads and saves project data.

	@arg	project_file	Project file to load. Leave empty to create a
				temporary project.
*/

Project::Project(QString project_file)
{
	// Setting the temporary status.
	_temporary = project_file.isEmpty();

	// If temporary, use some default data.
	if(_temporary)
	{
		// Setting the default project path and file.
		_path = QFileInfo("tmp").absoluteFilePath();
		_file = "";

		// Setting the project name.
		_name = "Temporary project";
		// Setting the default build options.
		_cc_opt = "-m3 -mb -O9 -Wall -Iinclude";
		_ld_opt = "-T addin.ld -nostdlib";
	}
	// Otherwise, use the parameter.
	else
	{
		// Setting the project path and file.
		_file = QFileInfo(project_file).absoluteFilePath();
		_path = QFileInfo(project_file).absolutePath();

		// Loading project file.
		load();
	}
}

/*
	Project::~Project()

	Frees a Project object. Deletes the allocated resources children.
*/

Project::~Project(void)
{
	// Using an iterator.
	int i;

	// Iterating over the project resources and deleting them.
	for(i=0; i<_resources.count(); i++) delete _resources[i];
}



/*
	Project::save()

	Saves the project data to the project file (directory name + ".pro").
	A temporary project hasn't a project file : data is kept only in the
	program RAM.
*/

void Project::save(void)
{
	// Getting the data and time as a string.
	QString datetime = QDateTime::currentDateTime().toString(
		"dd.MM.yyyy hh:mm");
	// Using an iterator.
	int i;

	// Don't do anything if the project is temporary.
	if(_temporary) return;

	printf("Saving project to '%s'\n", str(_file));

	// Opening the project file for writing.	
	QFile f(_file);
	if(!f.open(QFile::WriteOnly)) return;

	// Writing general information.
	f.write(QString(
		// Writing a commented header.
		"#\n"
		"#\tfxSDK project file\n"
		"#\tAutomatically generated on " + datetime + "\n"
		"#\n\n\n"

		// Writing project general information.
		"[General]\n"
		"name=" + _name + "\n"
		"cc_opt=" + _cc_opt + "\n"
		"ld_opt=" + _ld_opt + "\n"
		"\n\n"

		// Writing resource section indicator.
		"[Resources]\n"
	).toUtf8().constData());

	// Writing resource paths.
	for(i=0; i < _resources.count(); i++)
	{
		f.write(_resources[i]->path().toUtf8().constData());
		f.putChar('\n');
	}

	// Closing the project file.
	f.close();
}

/*
	Project::load()

	Loads the file project into the Project object. The project path must
	have been set before any loading operating can be performed.
*/

void Project::load(void)
{
	// Using a string to store a single line.
	char line[256];
	// Using a QFile object to read the project file.
	QFile f(_file);
	// Using a pointer to parse lines.
	const char *ptr;
	// Using an integer to store the parsing section. 0: nothing,
	// 1: general, 2: resources. And a counter.
	int section = 0, i;
	// Using strings to store parameter name and value.
	QString name, value;

	// Checking path validity.
	if(_file.isEmpty()) return;

	// Opening the file.
	if(!f.open(QFile::ReadOnly)) return;

	// Reading line by line.
	while(!f.atEnd())
	{
		// Reading a line.
		f.readLine(line, 256);

		/*
			Line interpretation.
		*/

		// Eliminating commented or empty lines.
		if(*line == '#' || isspace(*line)) continue;

		// Handling sections.
		if(*line == '[')
		{
			// Isolating the section name in [p; p+i[.
			ptr = line + 1;
			i = 0;
			while(ptr[i] != ']') i++;

			// Setting the section.
			if(!strncmp(ptr, "General", i)) section = 1;
			else if(!strncmp(ptr, "Resources", i)) section = 2;
			// If section name is incorrect, section is set to 0.
			else section = 0;

			// Continuing : the line has been totally analyzed.
			continue;
		}

		// Following interpretation needs to be in a section.
		if(!section) continue;

		// Getting rid of the last '\n'.
		line[strlen(line) - 1] = 0;

		// Handling parameters declaration.
		if(section == 1)
		{
			// Looking for '=' symbol.
			ptr = strchr(line, '=');
			if(!ptr) continue;

			// Isolating name and value.
			name = QString(QByteArray(line, ptr - line));
			value = QString(QByteArray(ptr + 1,
				strchr(line, 0) - (ptr + 1)));

			// Handling project name.
			if(name == "name") _name = value;
			// Handling build options.
			if(name == "cc_opt") _cc_opt = value;
			if(name == "ld_opt") _ld_opt = value;
		}

		// Handling resources : adding a resource.
		if(section == 2) addResource(QString(line));
	}

	// Closing the file.
	f.close();
}

/*
	Project::makefile()

	Generates the project Makefile in the project directory.
*/

void Project::makefile(void)
{
	// Using a string to store the name of the directory (file name).
	QString name = QFileInfo(_file).fileName();

	// Using a string list to store the names of object files.
	QStringList object_list;
	// Using strings to get object name list, the library list, the library
	// folder list and a generic string.
	QString objects, libs1 = "-Llib ", libs2, str;

	// Using a QFile object to write to the Makefile.
	QFile f(_path + "/Makefile");
	// Getting the data and time as a string.
	QString datetime = QDateTime::currentDateTime().toString(
		"dd.mm.yyyy hh:mm");

	// Using an iterator and a counter.
	int i, n;

	// Opening the file. Exiting on error.
	if(!f.open(QFile::WriteOnly)) return;



	/*
		Generating objects list and string and library strings.
	*/

	// Generating the object file list.
	for(i=0; i < _resources.count(); i++)
	{
		// Getting the resource type.
		enum Resource::Type type = _resources[i]->type();
		// Getting the file path info.
		QFileInfo info(_resources[i]->path());
		// Getting the file name.
		str = info.fileName();

		// Continuing if the file doesn't need any operation.
		if(type == Resource::Header || type == Resource::Other)
			continue;

		/*
			Handling libraries.
		*/

		if(type == Resource::Library)
		{
			if(!str.startsWith("lib")) continue;
			str = str.right(str.length() - 3);
			str = str.left(str.length()-info.suffix().length()-1);

			libs1 += "-L" + info.absolutePath() + " ";
			libs2 += "-l" + str + " ";

			continue;
		}

		/*
			By default, handling object files.
		*/

		// Adding it if the object file is not already in the list.
		if(!object_list.contains(str))
		{
			// Adding it to the list.
			object_list << str;
			// Continuing to loop.
			continue;
		}

		// Initializing counter.
		n = 1;

		// Looping until a valid name is found.
		while(object_list.contains(str + QString("_%1").arg(n))) n++;

		// Adding the final version to the list.
		object_list << str + QString("_%1").arg(n);
	}

	// Generating the object list string.
	for(i=0; i < object_list.count(); i++)
		// Adding the current object file to the string.
		objects += "build/" + object_list[i] + ".o ";

	// Removing the last spaces.
	objects = objects.left(objects.length() - 1);
	libs1 = libs1.left(libs1.length() - 1);
	libs2 = libs2.left(libs2.length() - 1);



	/*
		Writing Makefile data.
	*/

	f.write(QString(
		// Writing a commented header.
		"#\n"
		"#\tfxSDK project Makefile\n"
		"#\tAutomatically generated on " + datetime + "\n"
		"#\n\n\n"

		// Writing PHONY rule.
		".PHONY: all clean mrproper\n\n"

		// Writing variable information.
		"as      = sh3eb-elf-as\n"
		"cc      = sh3eb-elf-gcc\n"
		"cxx     = sh3eb-elf-g++\n"
		"objcopy = sh3eb-elf-objcopy\n"
		"wrapper = g1a-wrapper\n"
		"\n"
		"cflags  = " + _cc_opt + " " + libs1 + "\n"
		"ldflags = " + _ld_opt + " " + libs1 + " " + libs2 + "\n"
		"\n"
		"g1a = \"" + name + ".g1a\"\n"
		"bin = \"build/" + name + ".bin\"\n"
		"elf = \"build/" + name + ".elf\"\n"
		"obj = " + objects + "\n"
		"\n\n"

		// Writing generic rules information.
		"all: $(g1a)\n"
		"\t@ stat -c \"Output size is %s bytes.\" $(g1a)\n"
		"\n"
		"$(g1a): $(obj)\n"
		"\t$(cc) $(obj) -o $(elf) $(ldflags)\n"
		"\t$(objcopy) -R .comment -R .bss -O binary $(elf) $(bin)\n"
		"\t$(wrapper) $(bin) -o $(g1a) -i icon.bmp\n"
		"\n\n"
	).toUtf8().constData());

	// This is a counter that will iterate over the object_list array.
	n = 0;

	// Writing the specific rules.
	for(i=0; i < _resources.count(); i++)
	{
		// Using various strings.
		QString tool, options, target, dependency;
		// Using a variable to store the resource type.
		enum Resource::Type type = _resources[i]->type();

		// Continuing if the file hasn't an object.
		if(type == Resource::Header || type == Resource::Library
			|| type == Resource::Other) continue;

		// Only handle source files.
		if(type != Resource::Source)
		{
			// Still incrementing n.
			n++;
			// Continuing to next iteration.
			continue;
		}

		// Get the name of the original file.
		dependency = _resources[i]->path();
		// Getting the name of the target (object file).
		target = "build/" + object_list[n] + ".o";
		// Getting the compiled file extension.
		str = QFileInfo(dependency).suffix();
		// Getting the tool using the file extension.
		if(str == "s" || str == "S") tool = "$(as)";
		else if(str == "c") tool = "$(cc)";
		else tool = "$(cxx)";
		// Setting the options depending on the tool.
		if(tool != "$(as)") options = "$(cflags)";

		// Writing the formatted string.
		f.write(QString(
			target + ": " + dependency + "\n"
			"\t" + tool + " -c $^ -o $@ " + options + "\n"
			"\n"
		).toUtf8().constData());

		// An object has been used : going to the following one.
		n++;
	}

	// Adding clean and mrproper rules.
	f.write(
		"\n"
		"clean:\n"
		"\trm -f $(obj) $(elf) $(bin)\n"
		"\n"
		"mrproper: clean\n"
		"\trm -f $(g1a)\n"
	);

	// Closing the Makefile.
	f.close();
}



/*
	Project::isTemporary()

	Returns the temporary status of a project. A project is intended to be
	temporary when created, and to lose its temporary status forever when
	saved once.

	@return		True if temporary, false otherwise.
*/

bool Project::isTemporary(void)
{
	// Returning the temporary status as a boolean.
	return _temporary;
}



/*
	Project::path()

	Returns the project directory.

	@return		Project directory.
*/

QString Project::path(void)
{
	return _path;
}

/*
	Project::setPath()

	This function sets the path of a project. When a path is set, data from
	the original path is copied (but not deleted) and the temporary status
	is removed.
	This is the only way to remove a temporary status.
	The function also moves the resources file references that were in the
	temporary folder to get in the new folder.

	@arg	path	New path.
*/

void Project::setPath(QString path)
{
	// Using a shell command to copy data.
	QString cmd = "cp -r \"" + _path + "\"/* \"" + path + "\"";
	// Using a string to manipulate a resource path.
	QString res;
	// Using an iterator and an return value.
	int i, x;

	// Executing the copy.
	x = system(cmd.toUtf8().constData());
	if(x == -1) return;

	// Iterating over the resource paths.
	for(i=0; i < _resources.count(); i++)
	{
		// Getting the current resource's path.
		res = _resources[i]->path();

		// Testing if the current resource is in the directory. Adding
		// a '/' prevents contention between directories that begin
		// with the name of the current directory.
		if(res.startsWith(_path + "/"))
		{
			// Generating the new path.
			res.replace(0, _path.length(), path);
			// Setting the new path.
			_resources[i]->setPath(res);
		}
	}

	// Setting the new project path.
	_path = path;
	// Setting the new project file.
	_file = path + '/' + QFileInfo(path).fileName() + ".pro";
	// Removing the temporary status.
	_temporary = false;
}



/*
	Project::name()

	Returns the name of a project.

	@return		Project name.
*/

QString Project::name(void)
{
	// Returning the project name.
	return _name;
}

/*
	Project::setName()

	Sets the name of a project, if not empty.

	@arg	New name.
*/

void Project::setName(QString name)
{
	// Returning if the given name is empty.
	if(name.isEmpty()) return;

	// Setting the project name.
	_name = name;
}

/*
	Project::flags_cc()

	Returns the compiler options.

	@return		Compiler options string.
*/

QString Project::flags_cc(void)
{
	// Returning the attribute string.
	return _cc_opt;
}


/*
	Project::setFlags_cc()

	Changes the compiler options.

	@arg	cc_opt	Compiler options.
*/

void Project::setFlags_cc(QString cc_opt)
{
	// Updating the option attribute.
	_cc_opt = cc_opt;
}

/*
	Project::flags_ld()

	Returns the linker options.

	@return		Linker options string.
*/

QString Project::flags_ld(void)
{
	// Returning the attribute string.
	return _ld_opt;
}


/*
	Project::setFlags_ld()

	Changes the linker options.

	@arg	ld_opt	Linker options.
*/

void Project::setFlags_ld(QString ld_opt)
{
	// Updating the option attribute.
	_ld_opt = ld_opt;
}



/*
	Project::resources()

	Returns a list of resource pointers.

	@return		Resource list.
*/

QList<Resource *> Project::resources(void)
{
	// Returning the resource list.
	return _resources;
}

/*
	Project::addResource()

	Adds a resource to the list of a project's resources. A resource file
	cannot be added twice. Therefore, nothing is done if the file is
	already listed in the project's resources.

	@arg	path	File name.
*/

void Project::addResource(QString path)
{
	// Using a QFileInfo object.
	QFileInfo info(path);
	// Using a string to store the absolute path.
	QString absolute = QFileInfo(path).absoluteFilePath();
	// Using an iterator.
	int i;

	// Do not add directories : this doesn't make any sense.
	if(info.isDir()) return;

	// Iterating over the existing resources.
	for(i=0; i < _resources.count(); i++)
		// Returning if the resource already exists.
		if(_resources[i]->path() == absolute) return;

	// Adding the new resource.
	_resources << new Resource(absolute);
}

/*
	Project::removeResource()

	Removes a resource file from the project.

	@arg	path	File address.
*/

void Project::removeResource(QString path)
{
	// Using an iterator.
	int i = 0;

	// Iterating over the resources objects.
	while(i < _resources.count())
	{
		// For code safety, all the entries are removed, although a
		// resource should not be added multiple times (addResource()
		// ensures that). Removing the entries.
		if(_resources[i]->path() == path) _resources.removeAt(i);
		// Only increment i when no resource is removed (because the
		// list gets updated).
		else i++;
	}
}
