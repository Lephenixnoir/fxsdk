/*
	Header inclusions.
*/

#include "Highlighter.hpp"



/*
	Highlighter class definition.
*/

/*
	Highlighter::Highlighter()

	Constructs a Highlighter object. Loads the highlighting rules from file
	and stores them in the object memory.

	This syntax highlighter is driven by two types of highlighting rules.
	-	Single rules. These ones apply on only one line. Therefore,
		their scope is the block scope (highlightBlock() call).
	-	Multi-line rules. These ones apply on multiple lines. They
		use an id system width QTextBlock states.
	There's a specific type of single-line rules, that comes with an idea
	of "recursion" : rules that are only applied in specific multi-line
	blocks. Most common examples are string constants (such as \\ or %d)
	or documentation comments (such as @arg or @return).

	@arg	parent	Text document that will be using the highlighter.
*/

Highlighter::Highlighter(QTextDocument *parent, QString language_file)
	: QSyntaxHighlighter(parent)
{
	// Using regular expressions to parse the configuration file.
	// This one detects sections, of format '[SectionName]'.
	QRegExp section("\\s*\\[([^\\]]+)\\]\\s*");
	// This one parses a specific type of extended sections.
	QRegExp extend("SingleRules\\s*extending\\s*([^\\s\\]]+)");
	// This one detects a highlighting rule, which is represented by a set
	// of immediately-followed lines of format 'property: value'.
	QRegExp rule("([A-Za-z]+:\\s*[^\\n\\s]+\\n)+");
	// This last extracts a line from a rule.
	QRegExp line("([A-Za-z]+):\\s*([^\\s\\n]+)");

	// Using rules that will be constructed and added to the lists.
	struct SingleRule srule;
	struct MultiRule mrule;
	// Using integers to get the offsets of the matches and the current
	// offset in the file.
	int index_section, index_rule;
	int offset = 0;
	// Using an integer to store the current section type.
	// 0: Undefined, 1: Meta, 2: SingleRules, 3: MultiRules, 4: extended.
	// The other one stores the extended multi rule id when current = 4.
	int current = 0, current_id = 0;
	// Using an iterator.
	int i;

	// Opening the language file.
	QFile f(language_file);
	// Returning on file opening failure.
	if(!f.open(QFile::ReadOnly)) return;
	// Reading the file contents.
	QString text = f.readAll();
	// Closing the file.
	f.close();

	// Iterating over the sections and rules in the file contents.
	while(1)
	{
		index_section = section.indexIn(text, offset);
		index_rule = rule.indexIn(text, offset);

		if(index_section < 0 && index_rule < 0) break;

		if(index_section >= 0 && index_section <= index_rule)
		{
			QStringList sections = QStringList()
				<< "Meta" << "SingleRules" << "MultiRules";
			QString name = section.cap(1);

			offset = index_section + section.matchedLength();
			current = sections.indexOf(name) + 1;

			if(current) continue;

			if(extend.indexIn(name) != -1) current = 4;
			name = extend.cap(1);

			for(i = 0; i < multi_rules.count(); i++)
				if(multi_rules[i].name == name) break;
			current_id = i;

			continue;
		}

		switch(current)
		{
		case 0:
			break;

		case 1:
			if(rule.cap(0) == "lang") lang = rule.cap(1);
			break;

		case 2:
		case 4:
			i = 0;
			while((i = line.indexIn(rule.cap(0), i)) >= 0)
			{
				QString prop = line.cap(1), val = line.cap(2);

				if(prop == "name")
					srule.name = val;
				else if(prop == "regex")
					srule.pattern = QRegExp(val);
				else if(prop == "color")
					srule.format.setForeground(QColor(
					val));

				i += line.matchedLength();
			}

			if(current == 2) single_rules << srule;
			else multi_rules[current_id].single_rules << srule;

			break;

		case 3:
			i = 0;
			while((i = line.indexIn(rule.cap(0), i)) >= 0)
			{
				QString prop = line.cap(1), val = line.cap(2);

				if(prop == "name")
					mrule.name = val;
				else if(prop == "open")
					mrule.open = QRegExp(val);
				else if(prop == "close")
					mrule.close = QRegExp(val);
				else if(prop == "color")
					mrule.format.setForeground(QColor(
					val));

				i += line.matchedLength();
			}

			mrule.id = multi_rules.count();
			multi_rules << mrule;
			break;
		}

		offset = index_rule + rule.matchedLength();
	}
}



/*
	Highlighter::highlightBlock()

	Overloads the highlighting method of the QSytaxHighlighter. This one
	delegates work to two functions, for two reasons :
	-	highlightMulti() method is rather long and complex, so
		separating it is a good structure design choice.
	-	highlightSingle() method is called by highlightMulti() when
		recursion occurs, so it needed to be in a separated function.

	@arg	text	Block content, that will be highlighted.
*/

void Highlighter::highlightBlock(const QString &text)
{
	// Calling the single-line rules handler.
	highlightSingle(text, 0, text.length(), single_rules);
	// Calling the multi-line rules handler (overrides previous rules when
	// contention happens).
	highlightMulti(text);
}

/*
	Highlighter::highlightSingle()

	Highlights a single block (a single line) using the single-line rules
	of the object memory.

	@arg	text	Text to highlight.
	@arg	begin	Beginning of string part that will be highlighted
			(integer index of the first character).
	@arg	end	End index of highlighter section (first character
			outside).
	@arg	rules	Rule vector.
*/

void Highlighter::highlightSingle(const QString &text, int begin, int end,
	QVector<struct Highlighter::SingleRule> &rules)
{
	struct SingleRule srule;
	int i, j, n;
	int index, length;
	QRegExp pattern;

	for(i = 0; i < rules.count(); i++)
	{
		srule = rules[i];
		pattern = srule.pattern;

		n = begin;

		while((index = pattern.indexIn(text, n)) >= 0)
		{
			length = pattern.matchedLength();
			if(n + length > end) break;

			if(pattern.captureCount())
				for(j=1; j <= pattern.captureCount(); j++)
			{
				setFormat(index + pattern.pos(j),
				pattern.cap(j).length(), srule.format);
			}
			else
			{
				setFormat(index, length, srule.format);
			}

			n = index + length;
		}
	}
}

void Highlighter::highlightMulti(const QString &text)
{
	// Using a structure to store a multi-line rule data.
	struct MultiRule m;
	// Using integers to store the index of opening expression, the length
	// of string section that will be formatted, and the length of the
	// opening expression.
	int index, length, matched;
	// Using an integer offset to linearly parse the string content/
	int offset = 0;

	// Handling empty blocks.
	if(text.isEmpty())
	{
		// Inheriting the block state.
		setCurrentBlockState(previousBlockState());
		// Returning from function.
		return;
	}

	// Initializing the block state.
	setCurrentBlockState(-1);

	// Looping.
	while(1)
	{
		/*
			Determining the index and the rule of the incoming
			block.
		*/

		// Getting the currently opened rule if first.
		if(!offset && previousBlockState() != -1)
		{
			// If the current block has a state, using directly
			// the corresponding multi-line rule.
			m = multi_rules[previousBlockState()];

			// Setting the index.
			index = 0;

			matched = 0;
		}
		// Otherwise, looking for the first opened multi-line rule.
		else
		{
			// Storing the id and index of the first match.
			int first_index = text.length() + 1, first_id = -1;
			// Using an iterator and a temporary integer.
			int i, tmp;
			// Using a temporary multi-line rule.
			struct MultiRule mrule;

			// Iterating over the multi-line rules.
			for(i = 0; i < multi_rules.count(); i++)
			{
				// Getting the current rule.
				mrule = multi_rules[i];
				// Getting the index of first opening token.
				tmp = mrule.open.indexIn(text, offset);

				// If it's before the current best.
				if(tmp < first_index && tmp >= 0)
				{
					// Updating id and index.
					first_id = i;
					first_index = tmp;
					// Getting the length of opening
					// expression.
					matched = mrule.open.matchedLength();
				}
			}

			// If there's no opening expression anymore, breaking
			// the loop.
			if(first_id == -1) break;

			// Setting the rule structure.
			m = multi_rules[first_id];
			// Setting the index.
			index = first_index;
		}



		/*
			Setting the format for the block.
		*/

		// Finding the end of the rule.
		int endIndex = m.close.indexIn(text, index + matched);

		// If the rule has no end in this block, setting its state.
		if(endIndex == -1)
		{
			// Setting the block state.
			setCurrentBlockState(m.id);
			// Computing he length so that all the block end is
			// highlighted.
			length = text.length() - index;
		}
		// Otherwise, including the end expression to highlight it.
		else length = endIndex - index + m.close.matchedLength();

		// Settings the format.
		setFormat(index, length, m.format);

		if(m.single_rules.count()) highlightSingle(text,
			index, index + length + 1, m.single_rules);

		// Updating the offset.
		offset = index + length;
	}
}
