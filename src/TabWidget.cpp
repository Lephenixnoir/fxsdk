#include "TabWidget.hpp"

TabBar::TabBar(QWidget *parent)
	: QWidget(parent)
{
	w = 100;
	h = 24;

	setFixedHeight(h + 4);
	setMouseTracking(true);

	offset = 0;
	tabs_width = 0;

	tab_number = 0;
	tab_active = 0;
}

void TabBar::addTab(QString name)
{
	tabs << name;
	tab_number++;

	tabs_width = tab_number * (w+16) + 8;

	update();
}

void TabBar::selectTab(int index)
{
	tab_active = index;
	update();

	emit currentTabChanged(index);
}

void TabBar::closeTab(int index)
{
	tabs.removeAt(index);
	tab_number--;

	tabs_width = tab_number * (w+16) + 8;

	update();
}

int TabBar::tabNumber(void)
{
	return tabs.count();
}

void TabBar::paintEvent(QPaintEvent *)
{
	if(!tab_number) return;

	QPainter p(this);
	int i;

	adaptOffset();

	if(tabs_width > width())
	{
		int x = width()-28;
		QPoint points1[3] = { QPoint(x,16), QPoint(x+6,10), QPoint(x+6,22) };
		QPoint points2[3] = { QPoint(x+18,16), QPoint(x+12,10), QPoint(x+12,22) };
		p.setRenderHint(QPainter::Antialiasing,true);
		p.setBrush(QColor(0x4c,0x4c,0x4c));
		p.drawPolygon(points1,3);
		p.drawPolygon(points2,3);

		p.setClipRect(0,0,width() - 36,28);
	}

	for(i=0;i<tab_active;i++) renderTab(&p,i * (w+16) - offset);
	for(i=tab_number-1;i>=tab_active;i--) renderTab(&p,i * (w+16) - offset);

	p.setRenderHint(QPainter::Antialiasing,false);
	p.setPen(QColor(0x13,0x17,0x12));
	p.drawLine(0,height()-1,width()-1,height()-1);

	renderTab(&p,tab_active * (w + 16) - offset);

	p.setPen(QColor(0xbd,0xc0,0xc2,180));
	for(i=0;i<tab_number;i++) p.drawText(i*(w+16) + 12 - offset,4,w,24,Qt::AlignCenter,tabs[i]);
}

void TabBar::mousePressEvent(QMouseEvent *event)
{
	int y = event->y();
	if(y < 4) return;

	int x = event->x();
	int arrow_base = width() - 28;

	if(tabs_width > width() && x > arrow_base)
	{
		if(y < 10 || y > 22 || x > arrow_base +18) return;
		if(x < arrow_base + 9) offset -= 150;
		else offset += 150;

		adaptOffset();

		update();
		return;
	}

	int id = (x+offset-4)/(w+16);
	if(id >= tab_number) return;

	if(event->button() == Qt::LeftButton) selectTab(id);
	else if(event->button() == Qt::MidButton) emit closeTabRequested(id);
}

void TabBar::renderTab(QPainter *p, qreal x)
{
	qreal l[3] = { height()-h+.5, height()-h+8, (qreal)height() };

	QPainterPath path;
	path.moveTo(x,l[2]);
	path.lineTo(x+6,l[1]);
	path.quadTo(x+8,l[0],x+12,l[0]);
	path.lineTo(x+w+12,l[0]);
	path.quadTo(x+w+16,l[0],x+w+18,l[1]);
	path.lineTo(x+w+24,l[2]);

	p->setPen(QColor(0x15,0x19,0x16));
	p->setRenderHint(QPainter::Antialiasing,true);

	QLinearGradient g(0,0,0,28);
	g.setColorAt(0,QColor(0x55,0x59,0x56));
	g.setColorAt(0.25,QColor(0x55,0x59,0x56));
	g.setColorAt(0.3,QColor(0x2d,0x31,0x2e));
	g.setColorAt(1.0,QColor(0x25,0x29,0x26));

	p->setBrush(g);
	p->drawPath(path);
}

void TabBar::adaptOffset(void)
{
	if(tabs_width <= width()) { offset = 0; return; }
	if(offset < 0) offset = 0;
	if(offset > tabs_width - (width() - 36)) offset = tabs_width - (width() - 36);
}





TabWidget::TabWidget(QWidget *parent)
	: QWidget(parent)
{
	layout = new QVBoxLayout(this);
	layout->setMargin(0);

	tabBar = new TabBar(this);
	layout->addWidget(tabBar);

	stack = new QStackedWidget(this);
	layout->addWidget(stack);

	QAction *action_close = new QAction(this);
	action_close->setShortcut(Qt::CTRL | Qt::Key_W);
	connect(action_close,SIGNAL(triggered()),this,SLOT(closeTab()));
	addAction(action_close);

	connect(tabBar,SIGNAL(currentTabChanged(int)),stack,SLOT(setCurrentIndex(int)));
	connect(tabBar,SIGNAL(closeTabRequested(int)),this,SLOT(closeTab(int)));
}

TabWidget::~TabWidget(void)
{
	for(int i=0;stack->count();i++) closeTab(0);
}

void TabWidget::addTab(QString name, QWidget *widget)
{
	tabBar->addTab(name);
	stack->addWidget(widget);
	tabBar->selectTab(tabBar->tabNumber()-1);
}

int TabWidget::indexOf(QWidget *widget)
{
	return stack->indexOf(widget);
}

void TabWidget::closeTab(int index)
{
	if(index == -1) index = stack->currentIndex();

	QWidget *w = stack->widget(index);
	if(!w) return;
	if(w->isWindowModified())
	{
		QMessageBox b;
		b.setText("File has been edited.\n");
		b.setInformativeText("What do you want to do ?");
		b.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
		b.setDefaultButton(QMessageBox::Save);

		switch(b.exec())
		{
			case QMessageBox::Cancel: return;
		}
	}

	int current = stack->currentIndex();

	if(current == index)
	{
		int next = (stack->count()==1 ? -1 : (current ? current-1 : 0));
		stack->setCurrentIndex(next);
		tabBar->selectTab(next);
	}

	tabBar->closeTab(index);
	stack->removeWidget(w);
	delete w;
}

void TabWidget::paintEvent(QPaintEvent *)
{
	if(stack->count()) return;

	QPainter p(this);
	p.drawPixmap(-50,height()-340,QPixmap("logo.png"));
}
