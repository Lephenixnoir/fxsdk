#ifndef _ICON_HPP
	#define _ICON_HPP

/*
	Headers inclusion.
*/

#include <QColor>
#include <QIcon>
#include <QPainter>
#include <QPixmap>
#include <QString>



/*
	Icon namespace declaration.
*/

namespace Icon
{
	// The `__ICONSOURCE` macro is meant to be used id file `Icon.cpp` to
	// define the icon objects.
	#ifndef __ICONSOURCE
	extern
	#endif

	// Declaring the various icons.
	QIcon
		// File-related icons.
		NewFile, OpenFile,
		Save, SaveAs,
		Quit, Exit,
		// Edition-related icons.
		Search, Find,
		// Project-related icons.
		NewProject, OpenProject,
		Build, Settings,
		// View-related icons.
		Menu, Fullscreen,

		// Resource icons.
		Code, Header, Library, Image, Font, Other,
		// Other icons.
		Documentation, Remove;
	;

	// Initializes the icon module : loads the icons of the given file
	// with the given color and margin.
	void init(QString filepath, QColor color, int margin);
};

#endif // _ICON_HPP
