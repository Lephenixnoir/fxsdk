#ifndef _TABWIDGET_HPP
	#define _TABWIDGET_HPP

#include <QAction>
#include <QBitmap>
#include <QMessageBox>
#include <QMouseEvent>
#include <QPainter>
#include <QPaintEvent>
#include <QPixmap>
#include <QStackedWidget>
#include <QVBoxLayout>
#include <QWidget>

#include <QLabel>
#include <stdio.h>

class TabBar : public QWidget
{
Q_OBJECT

public:
	TabBar(QWidget *parent = 0);
	void addTab(QString name);
	void selectTab(int index);
	void closeTab(int index);
	int tabNumber(void);

signals:
	void currentTabChanged(int index);
	void closeTabRequested(int index);

protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);

private:
	void renderTab(QPainter *p, qreal x);
	void adaptOffset(void);
	qreal w, h;

	int offset;
	int tabs_width;

	int tab_number;
	int tab_active;
	QStringList tabs;
};

class TabWidget : public QWidget
{
Q_OBJECT

public:
	TabWidget(QWidget *parent = 0);
	~TabWidget(void);
	void addTab(QString name, QWidget *widget);
	int indexOf(QWidget *widget);

public slots:
	void closeTab(int index = -1);

protected:
	void paintEvent(QPaintEvent *event);

private:
	TabBar *tabBar;
	QWidget *shown;
	QStackedWidget *stack;
	QVBoxLayout *layout;
};

#endif // _TABWIDGET_HPP
