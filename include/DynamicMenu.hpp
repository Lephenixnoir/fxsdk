#ifndef _DYNAMICMENU_HPP
	#define _DYNAMICMENU_HPP

#include <QAction>
#include <QIcon>
#include <QKeySequence>
#include <QList>
#include <QMenu>
#include <QObject>
#include <QString>
#include <QWidget>

namespace DynamicCategory
{
	enum
	{
		Save    = 1,
		Edit    = 2,
		Project = 3
	};
};

class DynamicMenu : public QObject
{
public:
	enum ElementType
	{
		Menu      = 1,
		Separator = 2,
		Action    = 3,
		Category  = 4
	};

	struct Element
	{
		ElementType type;
		int category;
		union
		{
			QMenu *menu;
			QAction *action;
			int categoryId;
			void *pointer;
		};
	};

	DynamicMenu(QObject *parent = 0);

	void addAction(QAction *action, int category = 0);
	QAction *newAction(QString name, QObject *parent = 0, int category = 0);
	QAction *newAction(QIcon icon, QString name, QString shortcut="", QObject *parent = 0,
		int category = 0);

	void addSeparator(int category = 0);

	void addMenu(QMenu *menu, int category = 0);
	QMenu *newMenu(QWidget *parent = 0, int category = 0);
	QMenu *newMenu(QString name, QWidget *parent = 0, int category = 0);

	void addCategory(int categoryId);

	QList<struct Element> _elements(void);

private:
	QList<struct Element> elements;
};

#endif // _DYNAMICMENU_HPP
