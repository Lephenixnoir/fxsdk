#ifndef _DOCKWIDGET_HPP
	#define _DOCKWIDGET_HPP

#include <QAbstractScrollArea>
#include <QDockWidget>
#include <QBoxLayout>
#include <QLayout>
#include <QMouseEvent>
#include <QPainter>
#include <QPaintEvent>
#include <QPixmap>
#include <QResizeEvent>
#include <QString>
#include <QWidget>

class DockWidget : public QDockWidget
{
public:
	DockWidget(QString title, Qt::Orientation orientation, QWidget *parent);
	void addWidget(QWidget *widget);
	void addSpacing(int spacing);
	void addLayout(QLayout *layout);
	void setName(QString name);

private:
	class SubWidget : public QWidget
	{
	public:
		SubWidget(QString name, Qt::Orientation o, DockWidget *parent = 0);
		void addWidget(QWidget *widget);
		void addSpacing(int spacing);
		void addLayout(QLayout *layout);
		void setName(QString name);

	protected:
		void mousePressEvent(QMouseEvent *event);
		void resizeEvent(QResizeEvent *event);
		void paintEvent(QPaintEvent *event);

	private:
		DockWidget *_dock;
		QString _name;
		QWidget *_main;
		QBoxLayout *_layout;
	};

	SubWidget *sub;
};

#endif // _DOCKWIDGET_HPP
