#ifndef _PROJECT_HPP
	#define _PROJECT_HPP

/*
	Header inclusions.
*/

// Qt headers.
#include <QDateTime>
#include <QFile>
#include <QFileDialog>
#include <QList>
#include <QObject>
#include <QString>

// Standard headers.
#include <ctype.h>

// Custom headers.
#include "Resource.hpp"



// Temporary declaration. Simplifies debugging.
extern const char *str(QString str);



/*
	Project class definition.
*/

class Project
{
public:
	/*
		Constructor and destructor.
	*/

	Project(QString project_file = "");
	~Project(void);

	/*
		File interface methods.
	*/

	void save(void);
	void load(void);
	void makefile(void);

	/*
		Getters and setters.
	*/

	bool isTemporary(void);

	QString path(void);
	void setPath(QString path);

	QString name(void);
	void setName(QString name);

	QString flags_cc(void);
	void setFlags_cc(QString cc_options);
	QString flags_ld(void);
	void setFlags_ld(QString ld_options);

	/*
		Resource management.
	*/

	QList<Resource *> resources(void);
	void addResource(QString path);
	void removeResource(QString path);

private:
	// Temporary project flag indicator.
	bool _temporary;

	// Project path and file.
	QString _path;
	QString _file;

	// Project meta data.
	QString _name;
	QString _version;

	// Project parameters : assembler and compiler options.
	QString _cc_opt, _ld_opt;

	// Project resources.
	QList<Resource *> _resources;
};

#endif // _PROJECT_HPP
