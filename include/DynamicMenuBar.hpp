#ifndef _DYNAMICMENUBAR_HPP
	#define _DYNAMICMENUBAR_HPP

#include <QList>
#include <QMenuBar>
#include <QObject>
#include "DynamicMenu.hpp"

class DynamicMenuBar : public QObject
{
public:
/*
	enum MergingBehavior
	{
		IgnoreCategories = 1,
		MergeCategories  = 2
	};
*/

	DynamicMenuBar(QWidget *parent = 0);
	~DynamicMenuBar(void);

	void addMenu(DynamicMenu *menu, QString name = "");
	DynamicMenu *newMenu(QString name = "");
	void prepareMerge(DynamicMenu *menu/*, MergingBehavior = IgnoreCategories*/);
	QMenuBar *merge(void);
	QMenuBar *merge(DynamicMenu *menu);

private:
	void addToMenu(QMenu *menu, DynamicMenu::Element el);
	QMenuBar *currentMenuBar;

	struct Menu
	{
		DynamicMenu *pointer;
		QString name;
	};
	QList<struct Menu> menus;

	struct MergeOperation
	{
		DynamicMenu *pointer;
//		MergingBehavior behavior;
	};
	QList<struct MergeOperation> operation;
};

#endif // _DYNAMICMENUBAR_HPP
