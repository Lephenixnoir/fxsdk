#ifndef _EDITORFONT_HPP
	#define _EDITORFONT_HPP

#include "EditorWidget.hpp"

class EditorFont : public EditorWidget
{
Q_OBJECT

public:
	EditorFont(QWidget *parent, QString resource);

public slots:
	virtual void setChanged(bool hasChanged = true);
	virtual void save(bool saveas = false);
	void saveAs(void);

signals:
	void nameChanged(QString new_name);
};

#endif // _EDITORFONT_HPP
