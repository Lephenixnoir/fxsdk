#ifndef _PROJECTMANAGER_HPP
	#define _PROJECTMANAGER_HPP

#include <QDir>
#include <QFileInfo>
#include <QList>
#include <QObject>
#include <QProcess>

#include "Project.hpp"

class ProjectManager : public QObject
{
Q_OBJECT

public:
	ProjectManager(QObject *parent = 0);
	~ProjectManager(void);
	Project *currentProject(void);

public slots:
	void cleanTemporaryProject(void);

	void newProject(void);
	void openProject(void);
	void addFile(QString file);
	void addFiles(void);
	void rebuildAll(void);
	void build(void);

	void buildOutput(void);

signals:
	void buildOutputAvailable(QString build_output);
	void changed(Project *current_project);

private:
	Project *_current;
	QProcess *process;
};

#endif // _PROJECTMANAGER_HPP
