#ifndef _EDITORSET_HPP
	#define _EDITORSET_HPP

#include <QAbstractButton>
#include <QAction>
#include <QEvent>
#include <QFocusEvent>
#include <QKeyEvent>
#include <QList>
#include <QMessageBox>
#include <QMouseEvent>
#include <QPainter>
#include <QPaintEvent>
#include <QResizeEvent>
#include <QString>
#include <QWidget>

#include "DynamicMenu.hpp"
#include "EditorWidget.hpp"
#include "Icon.hpp"

/*
	EditorSet

	The editor widget handler, which looks like a tab widget. Will possibly
	have different customizable appearances. Uses no subclass to centralize
	data.

	@inherits	public QWidget
*/

class EditorSet : public QWidget
{
Q_OBJECT

public:
	// Generic constructor.
	EditorSet(QWidget *parent = 0);
	// Destructor.
	~EditorSet(void);
	// Adding widgets and editors.
	QWidget *addWidget(QWidget *widget, QString name = "");
	EditorWidget *addEditor(EditorWidget *widget, QString name = "");
	// Selecting widgets and editors.
	void setActive(int tab_number, bool previous_still_exists = true);
	// Retrieving the editor set dynamic menu.
	DynamicMenu *dynamicMenu(void);
	// Retrieving dynamic menus.
	DynamicMenu *dynamicMenuFor(QObject *pointer);
	// Testing if files are opened (focuses if file is opened).
	bool isOpened(QString path);

public slots:
	// Closing the current editor.
	void closeCurrentEditor(void);
	// Updating editor names.
	void updateEditorName(QString new_name);
	// Adding the current file to project.
	void addCurrentFile(void);

signals:
	// Focused editor changed.
	void focused(DynamicMenu *);
	// Trying to add current file to project.
	void addToProjectRequest(QString resource);

protected:
	// Re-implemented events handlers.
	void resizeEvent(QResizeEvent *event);
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void keyPressEvent(QKeyEvent *event);

private slots:
	// Additional method to change editor.
	void switchEditor(int displacement = 0);

private:
	// Control tab overflow.
	bool tabs_overflow(void);
	// Getting the tab at any position.
	int tab_at(int x);
	// Rendering individual tabs.
	void renderTab(QPainter *painter, int i, int active);
	// Updating actions visibility.
	void updateActions(void);

	/*
		EditorSet::EditorSetArrow

		An arrow inheriting QAbstractButton that highlights on hover
		to select another tab. Can be oriented right or left (see
		Orientation enumeration).

		@inherits	QAbstractButton
	*/

	class EditorSetArrow : public QAbstractButton
	{
	public:
		// Public orientation enumeration.
		enum Orientation
		{
			Left  = 1,
			Right = 2
		};

		// Generic constructor. Orientation cannot be changed.
		EditorSetArrow(Orientation orientation, QWidget *parent = 0);

	protected:
		// Re-implemented event handlers.
		void paintEvent(QPaintEvent *);
		void enterEvent(QEvent *event);
		void leaveEvent(QEvent *event);

	private:
		// Arrow orientation.
		Orientation o;
		// Hover state.
		bool hovered;
	};

	/*
		EditorSet::Element

		A simple structure that made it possible to add classical
		widgets to the editor set.
	*/

	struct Element
	{
		// Element union.
		union
		{
			QWidget *widget;
			EditorWidget *editor;
		};

		// Is it an editor ?
		bool isEditor;
		// Element name.
		QString name;
	};

	// Element list.
	QList<struct Element> elements;
	// Width and height of tabs. Not really customizable.
	qreal w, h;
	// Identifier of the current element.
	int active;

	// Tab switching arrows.
	EditorSetArrow *arrowl, *arrowr;
	// Dynamic editor set menu.
	DynamicMenu *menu;
	// Dynamic menu actions.
	QAction *action_add;
};

#endif // _EDITORSET_HPP
