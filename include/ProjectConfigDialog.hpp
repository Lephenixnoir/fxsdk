#ifndef _PROJECTCONFIGFIALOG_HPP
	#define _PROJECTCONFIGFIALOG_HPP

#include <QDialog>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QWidget>

#include "Project.hpp"

class ProjectConfigDialog : public QDialog
{
Q_OBJECT

public:
	ProjectConfigDialog(Project *project, QWidget *parent = 0);

public slots:
	void check(void);
	void write(void);

private:
	Project *_project;

	QLineEdit *line_name, *line_ccopt, *line_ldopt;
	QDialogButtonBox *buttons;
};

#endif // _PROJECTCONFIGFIALOG_HPP
