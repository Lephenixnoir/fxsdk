#ifndef _PROJECTVIEW_HPP
	#define _PROJECTVIEW_HPP

#include <QAction>
#include <QContextMenuEvent>
#include <QIcon>
#include <QLabel>
#include <QLayoutItem>
#include <QListWidget>
#include <QMenu>
#include <QPushButton>
#include <QString>
#include <QVBoxLayout>
#include <QWidget>

#include "DockWidget.hpp"
#include "Icon.hpp"
#include "Project.hpp"
#include "ProjectConfigDialog.hpp"
#include "Resource.hpp"

extern const char *str(QString str);

class ProjectView : public DockWidget
{
Q_OBJECT

public:
	ProjectView(QWidget *parent = 0);

public slots:
	void updateView(Project *project);
	void configure(void);
	void saveProject(void);

	// Public slots... but just thought to be called by the resource
	// widgets. Trying to find a more proper way for that...
	void openFile(QString path, enum Resource::Type type);
	void removeResource(QString path);

signals:
	void fileOpenRequest(QString path, enum Resource::Type type);

private:
	class CategoryWidget : public QWidget
	{
	public:
		CategoryWidget(QIcon icon, QString name, QWidget *parent = 0);
	protected:
		void paintEvent(QPaintEvent *event);
	private:
		QIcon _icon;
		QString _name;
	};

	Project *_project;

	QVBoxLayout *_layout;
	QList<QWidget *> resw;
	QList<CategoryWidget *> ctgys;
	CategoryWidget *ctgy_sources, *ctgy_headers, *ctgy_libs, *ctgy_images,
		*ctgy_fonts, *ctgy_other;

	QPushButton *saveButton;
};



/*
	ResourceWidget class definition.
*/

class ResourceWidget : public QLabel
{
Q_OBJECT

public:
	// Simple constructor using resource. The widget can only be attached
	// to a ProjectView because it uses its functionalities.
	ResourceWidget(Resource *resource, QWidget *parent = 0);

public slots:
	void remove(void);

signals:
	void openFileRequest(QString path, enum Resource::Type type);
	void resourceRemoveRequest(QString path);

protected:
	void mouseDoubleClickEvent(QMouseEvent *event);
	void contextMenuEvent(QContextMenuEvent *event);

private:
	Resource *_resource;
};

#endif // _PROJECTVIEW_HPP
