#ifndef _STYLEPARSER_HPP
	#define _STYLEPARSER_HPP

#include <QFile>
#include <QList>
#include <QString>
#include <QStringList>

class StyleSheet
{
public:
	struct Variable
	{
		QString name;
		QString value;
	};

	struct Theme
	{
		QString name;
		QList<struct Variable> vars;
	};

	StyleSheet(QString rules = "");
	void addTheme(struct Theme t);
	void addRules(QString rules);
	QString theme(QString theme);

private:
	QList<struct Theme> themes;
	QString rules;
};

class StyleParser
{
public:
	static StyleSheet parse(QString filepath);

private:
	static struct StyleSheet::Variable parseThemeLine(QString line);
};

#endif // _STYLEPARSER_HPP
