#ifndef _MAINWINDOW_HPP
	#define _MAINWINDOW_HPP

#include <QAction>
#include <QApplication>
#include <QDialogButtonBox>
#include <QDockWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QMainWindow>
#include <QMenuBar>
#include <QPushButton>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QWidget>

#include "DockWidget.hpp"
#include "DynamicMenuBar.hpp"
#include "EditorFont.hpp"
#include "EditorSet.hpp"
#include "EditorText.hpp"
#include "Icon.hpp"
#include "ProjectConfigDialog.hpp"
#include "ProjectManager.hpp"
#include "ProjectView.hpp"
#include "TabWidget.hpp"

/**
 *	MainWindow
 *	@descr	The main window class, that should be shown at every time during the program
 *			execution. Only one object is intended to be instantiated.
 *
 *	@inherits		public QMainWindow
 */

class MainWindow : public QMainWindow
{
Q_OBJECT

public:
	// Default constructor. Requires no parameters.
	MainWindow(void);

public slots:
	// `File` menu slots.
	void file_new(void);
	void file_open(void);
	// `View` menu slots.
	void view_menu(void);
	void view_fullscreen(void);

	// Menu bar can be updated, for instance, when project changes.
	void updateMenuBar(void);
	// The main window's dynamic menu changes with focus.
	void setDynamicMenu(DynamicMenu *menu);
	// Empty compiler output.
	void outputBuildEmpty(void);
	// Update compiler output.
	void outputBuildAppend(QString str);

	void project_fileOpenRequest(QString path, enum Resource::Type type);


	void updateProjectView(void);

private:
	// Subroutine for setting up menu bar.
	void setupMenuBar(void);

	// Editor widget.
	EditorSet *editors;
	// Dynamic menu bar.
	DynamicMenuBar *menubar;
	// Main window's dynamic menu.
	DynamicMenu *dynamicMenu;
	// Dynamic menu for project handling (specific).
	DynamicMenu *dynamicProject;
	// Project manager.
	ProjectManager *projectManager;
	// Project view dock widget.
	ProjectView *projectView;
	// Build output text area.
	QPlainTextEdit *output_edit;
};

#endif // _MAINWINDOW_HPP
