#ifndef _CODEEDIT_HPP
	#define _CODEEDIT_HPP

#include <QAction>
#include <QObject>
#include <QPainter>
#include <QPaintEvent>
#include <QPalette>
#include <QPlainTextEdit>
#include <QResizeEvent>
#include <QSize>
#include <QTextBlock>
#include <QWidget>

#include "Highlighter.hpp"

class LinesArea;

class CodeEdit : public QPlainTextEdit
{
Q_OBJECT

public:
	CodeEdit(QWidget *parent, QString language_file = "");
	void linesPaintEvent(QPaintEvent *event);

public slots:
	void changeSyntaxHighlighter(QString language_file);

protected:
	void resizeEvent(QResizeEvent *event);

private slots:
	void updateLinesAreaWidth(int blocks = 0);
	void highlightCurrentLine(void);
	void updateLineNumberArea(QRect r, int dy);

private:
	int linesAreaWidth(void);
	LinesArea *lines;
	Highlighter *highlighter;
};

class LinesArea : public QWidget
{
public:
	LinesArea(CodeEdit *editor);

protected:
	void paintEvent(QPaintEvent *event);

private:
	CodeEdit *editor;
};

#endif // _CODEEDIT_HPP
