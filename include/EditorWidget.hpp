#ifndef _EDITORWIDGET_HPP
	#define _EDITORWIDGET_HPP

#include <QFileDialog>
#include <QString>
#include <QWidget>

#include "DynamicMenu.hpp"

class EditorWidget : public QWidget
{
public:
	EditorWidget(QWidget *parent, QString resourcePath = "");
	bool changed(void);
	DynamicMenu *dynamicMenu(void);
	QString file(void);
	virtual void save(bool saveas = false) = 0;

public slots:
	virtual void setChanged(bool hasChanged = true);

signals:
	void nameChanged(QString new_name);

protected:
	void setDynamicMenu(DynamicMenu *new_menu);
	void setResource(QString new_resource);
	QString openFile(bool saveas);

private:
	bool _changed;
	QString _resource;
	DynamicMenu *_menu;
};

#endif // _EDITORWIDGET_HPP
