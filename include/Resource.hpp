#ifndef _RESOURCE_HPP
	#define _RESOURCE_HPP

/*
	Header inclusions.
*/

#include <QFileInfo>
#include <QString>



/*
	Resource class definition.
*/

class Resource
{
public:
	// This enumeration handles a resource type.
	enum Type
	{
		Source = 0,
		Header,
		Library,
		Image,
		Font,

		Other
	};

	// Constructor, from resource path.
	Resource(QString path);

	// Path getter and setter.
	QString path(void);
	void setPath(QString new_path);

	// Type getter and setter.
	enum Type type(void);
	void setType(enum Type type);

private:
	// Resource file path.
	QString _path;
	// Resource type.
	enum Type _type;
};

#endif // _RESOURCE_HPP
