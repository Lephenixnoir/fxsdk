#ifndef _EDITORTEXT_HPP
	#define _EDITORTEXT_HPP

#include <QAction>
#include <QMenu>
#include <QVBoxLayout>

#include "CodeEdit.hpp"
#include "Icon.hpp"
#include "EditorWidget.hpp"

class EditorText : public EditorWidget
{
Q_OBJECT

public:
	EditorText(QWidget *parent, QString resource);

public slots:
	virtual void setChanged(bool hasChanged = true);
	virtual void save(bool saveas = false);
	void saveAs(void);
	void find(void);

signals:
	void nameChanged(QString new_name);

private:
	void setSyntaxHighlighter(QString resource);

	CodeEdit *edit;
};

#endif // _EDITORTEXT_HPP
