#ifndef _HIGHLIGHTER_HPP
	#define _HIGHLIGHTER_HPP

#include <QBrush>
#include <QFile>
#include <QRegExp>
#include <QString>
#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QTextDocument>

#include <stdio.h>

class Highlighter : public QSyntaxHighlighter
{
Q_OBJECT

public:
	Highlighter(QTextDocument *document, QString language_file);

private:
	struct SingleRule
	{
		QString name;
		QRegExp pattern;
		QTextCharFormat format;
	};

	struct MultiRule
	{
		QString name;
		int id;

		QRegExp open, close;
		QTextCharFormat format;

		QVector<struct SingleRule> single_rules;
	};

protected:
	void highlightBlock(const QString &text);
	void highlightSingle(const QString &text, int begin, int end,
		QVector<struct SingleRule> &rules);
	void highlightMulti(const QString &text);

private:
	QString lang;
	QVector<struct SingleRule> single_rules;
	QVector<struct MultiRule> multi_rules;
};

#endif // _HIGHLIGHTER_HPP
